import { Injectable } from '@angular/core';
import { WordListItem } from 'src/app/words-table/components/word-list-tile/interfaces/word-list-item.interface';

@Injectable()
export class WordListItemModel implements WordListItem {
    wordId: string;
    text: string;
    translations: string[];
    status: string;

    constructor(init?: Partial<WordListItemModel>) {
        Object.assign(this, init);
    }
}
