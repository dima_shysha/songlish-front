export class WordDetailsModel {
    wordId: string;
    translations: string[];

    constructor(init?: Partial<WordDetailsModel>) {
        Object.assign(this, init);
    }
}
