export class UpdateWordModel {
    wordId: string;
    translations: string[];

    constructor(init?: Partial<UpdateWordModel>) {
        Object.assign(this, init);
    }
}
