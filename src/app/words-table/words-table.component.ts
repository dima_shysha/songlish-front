import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import {
    ChangeWordStatus,
    GetWords,
    SetWordsFilter,
    SetWordsSearchQuery,
    UpdateWord,
} from './words-state/words.actions';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { WordDetailsModalComponent } from './components/word-details-modal/word-details-modal.component';
import { WordDetails } from './components/word-details-modal/models/word-details.interface';
import { SubSink } from 'subsink';
import { WordListItemModel } from './models/word-list-item.model';
import { TrainingMode } from '../training-selection-page/models/enums/training-mode.enum';
import { WordsFilters } from './enums/words-filters.enum';
import { WordsQuery } from './words-state/words.query';
import { Navigate } from '@ngxs/router-plugin';

@Component({
    selector: 'sng-words-table',
    templateUrl: './words-table.component.html',
    styleUrls: ['./words-table.component.scss'],
})
export class WordsTableComponent implements OnInit, OnDestroy {
    @ViewChild('searchInput') searchInput: ElementRef;

    @Select(WordsQuery.words) words$: Observable<WordListItemModel[]>;

    filterValue: string;

    expandedItemId: string;

    TrainingMode = TrainingMode;

    isLoading = true;

    noWordsAdded = false;

    protected sub = new SubSink();

    constructor(
        private store: Store,
        private dialog: MatDialog,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.sub.add(
            this.store.dispatch(new GetWords()).subscribe(x => {
                this.noWordsAdded = !x.words.items.length;
                this.isLoading = false;
                this.cdr.detectChanges();
            })
        );
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    onListItemSelect(event: WordListItemModel): void {
        this.dialog
            .open<
                WordDetailsModalComponent,
                WordDetailsModalComponent,
                WordDetails
            >(WordDetailsModalComponent, {
                data: {
                    model: event,
                },
                width: 'min(400px, 90vw)',
            } as MatDialogConfig<WordDetailsModalComponent>)
            .afterClosed()
            .subscribe(x => {
                this.store.dispatch(new UpdateWord({ ...x }));
            });
    }

    onChangeStatus(status: string, id: string) {
        this.store.dispatch(new ChangeWordStatus(id, status));
    }

    onFilterValueChanged(event: WordsFilters): void {
        this.store.dispatch(new SetWordsFilter(event));
    }

    onSearchInput(query: string): void {
        this.store.dispatch(new SetWordsSearchQuery(query));
    }

    onGoToSongAdding(): void {
        this.store.dispatch(new Navigate(['song-search']));
    }
}
