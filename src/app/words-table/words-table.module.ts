import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordsTableComponent } from './words-table.component';
import { NgxsModule } from '@ngxs/store';
import { WordsState } from './words-state/words.state';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { WarningDialogModule } from 'src/app/_shared/warning-dialog/warning-dialog.module';
import { MatListModule } from '@angular/material/list';
import { WordDetailsModalModule } from './components/word-details-modal/word-details-modal.module';
import { WordListTileModule } from './components/word-list-tile/word-list-tile.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { StatisticsBtnModule } from './components/statistics-btn/statistics-btn.module';
import { WordsFilterBtnModule } from './components/words-filter-btn/words-filter-btn.module';
import { SearchFieldModule } from './components/search-field/search-field.module';
import { WordsApiService } from './words-state/services/words-api.service';
import { LoadingSpinnerModule } from '../_shared/loading-directive/loading-spinner/loading-spinner.module';
import { WordsTableRoutingModule } from './words-table-routing.module';

@NgModule({
    declarations: [WordsTableComponent],
    providers: [WordsApiService],
    imports: [
        WordsTableRoutingModule,
        NgxsModule.forFeature([WordsState]),
        WordDetailsModalModule,
        WordListTileModule,
        StatisticsBtnModule,
        SearchFieldModule,
        WordsFilterBtnModule,
        WarningDialogModule,
        LoadingSpinnerModule,
        ScrollingModule,
        CommonModule,
        MatChipsModule,
        MatFormFieldModule,
        DragDropModule,
        MatExpansionModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
    ],
    exports: [WordsTableComponent],
})
export class WordsTableModule {}
