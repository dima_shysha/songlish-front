import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WordsTableComponent } from './words-table.component';

const routes: Routes = [
    {
        path: '',
        component: WordsTableComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class WordsTableRoutingModule {}
