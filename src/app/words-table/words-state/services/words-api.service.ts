import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UpdateWordModel } from '../../models/update-word.model';
import { WordDetailsModel } from '../../models/word-details.model';
import { WordListItemModel } from '../../models/word-list-item.model';

@Injectable()
export class WordsApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getAll(): Observable<WordListItemModel[]> {
        return this.http
            .get<WordListItemModel[]>(`${this.url}/dictionary/words`)
            .pipe(
                map(items =>
                    items.sort((x1, x2) => x2.text.localeCompare(x1.text))
                )
            );
    }

    changeWordStatus(id: string, status: string): Observable<any> {
        return this.http.put(`${this.url}/dictionary`, [
            {
                wordId: id,
                wordStatus: status,
            },
        ]);
    }

    // param: id
    delete(_: string): Observable<number> {
        return of(1);
    }

    update(model: UpdateWordModel): Observable<UpdateWordModel> {
        return this.http
            .post<WordDetailsModel[]>(`${this.url}/dictionary`, [model])
            .pipe(
                map(() => model),
                catchError(_ => of(null))
            );
    }
}
