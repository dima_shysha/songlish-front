import { WordsFilters } from '../enums/words-filters.enum';
import { WordListItemModel } from '../models/word-list-item.model';

export class GetWords {
    static readonly type = '[Words] Get words';
}

export class DeleteWord {
    static readonly type = '[Words] Delete word';

    constructor(public id: string) {}
}

export class UpdateWord {
    static readonly type = '[Words] Update word';

    constructor(public model: WordListItemModel) {}
}

export class ChangeWordStatus {
    static readonly type = '[Words] Change word status';

    constructor(public id: string, public status: string) {}
}

export class SetWordsFilter {
    static readonly type = '[Words] Set words filter';

    constructor(public filter: WordsFilters) {}
}

export class SetWordsSearchQuery {
    static readonly type = '[Words] Set words search query';

    constructor(public query: string) {}
}
