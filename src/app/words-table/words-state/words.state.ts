import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { map, Observable, tap } from 'rxjs';
import { WordsApiService } from './services/words-api.service';
import {
    ChangeWordStatus,
    DeleteWord,
    GetWords,
    SetWordsFilter,
    SetWordsSearchQuery,
    UpdateWord,
} from './words.actions';
import { patch, removeItem, updateItem } from '@ngxs/store/operators';
import { WordDetailsModel } from '../models/word-details.model';
import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { WordListItemModel } from '../models/word-list-item.model';
import { WordsFilters } from '../enums/words-filters.enum';
import { UpdateWordModel } from '../models/update-word.model';

export class WordsStateModel {
    items: WordListItemModel[];
    itemsFilter: WordsFilters;
    searchQuery: string;
}

const defaults = {
    items: [],
    itemsFilter: null,
    searchQuery: null,
} as WordsStateModel;

@State<WordsStateModel>({
    name: 'words',
    defaults,
})
@Injectable()
export class WordsState {
    constructor(private wordsApiService: WordsApiService) {}

    @Selector()
    static items(state: WordsStateModel): WordListItemModel[] {
        return state.items;
    }

    @Selector()
    static itemsFilter(state: WordsStateModel): WordsFilters {
        return state.itemsFilter;
    }

    @Selector()
    static itemsSearchQuery(state: WordsStateModel): string {
        return state.searchQuery;
    }

    @Action(GetWords)
    onGetWords({ patchState }: StateContext<WordsStateModel>) {
        return this.wordsApiService.getAll().pipe(
            map(x =>
                x.sort((a, _) => {
                    if (a.status === WORD_STATUS.onStudy) {
                        return -1;
                    }

                    return 0;
                })
            ),
            tap(items => {
                patchState({ items });
            })
        );
    }

    @Action(SetWordsFilter)
    onSetWordsFilter(
        { patchState }: StateContext<WordsStateModel>,
        { filter }: SetWordsFilter
    ) {
        patchState({ itemsFilter: filter });
    }

    @Action(SetWordsSearchQuery)
    onSetWordsSearchQuery(
        { patchState }: StateContext<WordsStateModel>,
        { query }: SetWordsSearchQuery
    ) {
        patchState({ searchQuery: query });
    }

    @Action(DeleteWord)
    onDeleteWord(
        { setState }: StateContext<WordsStateModel>,
        { id }: DeleteWord
    ) {
        return this.wordsApiService.delete(id).pipe(
            tap(_ => {
                setState(
                    patch({
                        items: removeItem<WordListItemModel>(
                            x => x.wordId === id
                        ),
                    })
                );
            })
        );
    }

    @Action(UpdateWord)
    onUpdateWord(
        { setState }: StateContext<WordsStateModel>,
        { model }: UpdateWord
    ): Observable<WordDetailsModel> {
        return this.wordsApiService.update(model).pipe(
            tap((item: UpdateWordModel) => {
                setState(
                    patch({
                        items: updateItem<WordListItemModel>(
                            x => x.wordId === item.wordId,
                            { ...model, ...item }
                        ),
                    })
                );
            })
        );
    }

    @Action(ChangeWordStatus)
    onChangeWordStatus(
        { setState }: StateContext<WordsStateModel>,
        { id, status }: ChangeWordStatus
    ): Observable<WordDetailsModel> {
        return this.wordsApiService.changeWordStatus(id, status).pipe(
            tap(x => {
                setState(
                    patch({
                        items: updateItem<WordListItemModel>(
                            x => x.wordId === id,
                            patch<WordListItemModel>({
                                status,
                            })
                        ),
                    })
                );
            })
        );
    }
}
