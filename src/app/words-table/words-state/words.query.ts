import { Selector } from '@ngxs/store';
import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { WordsFilters } from '../enums/words-filters.enum';
import { WordListItemModel } from '../models/word-list-item.model';
import { WordsState } from './words.state';

export class WordsQuery {
    @Selector([
        WordsState.items,
        WordsState.itemsFilter,
        WordsState.itemsSearchQuery,
    ])
    static words(
        items: WordListItemModel[],
        itemsFilter: WordsFilters,
        itemsSearchQuery: string
    ): WordListItemModel[] {
        if (itemsSearchQuery) {
            const preparedQuery = itemsSearchQuery.trim().toLowerCase();
            items = items.filter(x =>
                x.text.trim().toLowerCase().includes(preparedQuery)
            );
        }

        if (itemsFilter !== null && itemsFilter !== undefined) {
            switch (itemsFilter) {
                case WordsFilters.STUDIED:
                    return items.filter(x => x.status === WORD_STATUS.studied);
                case WordsFilters.ON_STUDY:
                    return items.filter(x => x.status === WORD_STATUS.onStudy);
                case WordsFilters.ALL:
                    return items;
                default:
                    return items;
            }
        }

        return items;
    }
}
