import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordsFilterBtnComponent } from './words-filter-btn.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [WordsFilterBtnComponent],
    imports: [CommonModule, MatButtonModule, MatIconModule, MatMenuModule],
    exports: [WordsFilterBtnComponent],
})
export class WordsFilterBtnModule {}
