import { Component, EventEmitter, Output } from '@angular/core';
import { WordsFilters } from '../../enums/words-filters.enum';

@Component({
    selector: 'sng-words-filter-btn',
    templateUrl: './words-filter-btn.component.html',
    styleUrls: ['./words-filter-btn.component.scss'],
})
export class WordsFilterBtnComponent {
    @Output() changed = new EventEmitter<WordsFilters>();

    WordsFilters = WordsFilters;

    onItemSelect(value: WordsFilters): void {
        this.changed.emit(value);
    }
}
