import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WordsFilterBtnComponent } from './words-filter-btn.component';

describe('WordsFilterBtnComponent', () => {
    let component: WordsFilterBtnComponent;
    let fixture: ComponentFixture<WordsFilterBtnComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [WordsFilterBtnComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(WordsFilterBtnComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
