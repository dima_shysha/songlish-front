import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsModalComponent } from './statistics-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { SubSink } from 'subsink';
import { StatisticsApiService } from './services/statistics-api.service';
import { MatIconModule } from '@angular/material/icon';
import { LoadingSpinnerModule } from 'src/app/_shared/loading-directive/loading-spinner/loading-spinner.module';

@NgModule({
    declarations: [StatisticsModalComponent],
    providers: [StatisticsApiService, SubSink],
    imports: [
        CommonModule,
        MatDialogModule,
        MatIconModule,
        LoadingSpinnerModule,
    ],
    exports: [StatisticsModalComponent],
})
export class StatisticsModalModule {}
