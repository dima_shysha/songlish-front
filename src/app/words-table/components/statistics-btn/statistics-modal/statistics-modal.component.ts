import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { StatisticsModel } from './models/statistics.model';
import { StatisticsApiService } from './services/statistics-api.service';

@Component({
    selector: 'sng-statistics-modal',
    templateUrl: './statistics-modal.component.html',
    styleUrls: ['./statistics-modal.component.scss'],
})
export class StatisticsModalComponent implements OnInit, OnDestroy {
    statistics: StatisticsModel;

    isLoading = true;

    constructor(
        private statisticsApiService: StatisticsApiService,
        private sub: SubSink
    ) {}

    ngOnInit(): void {
        this.sub.add(
            this.statisticsApiService.getStatistics().subscribe(x => {
                this.statistics = x;
                this.isLoading = false;
            })
        );
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
}
