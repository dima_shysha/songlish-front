import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StatisticsModel } from '../models/statistics.model';

@Injectable()
export class StatisticsApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getStatistics(): Observable<any> {
        return this.http.get<StatisticsModel>(`${this.url}/user/statistic`);
    }
}
