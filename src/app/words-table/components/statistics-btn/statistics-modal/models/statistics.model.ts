export class StatisticsModel {
    countWords: number;
    countStudiedWords: number;
    countStudyWords: number;
    countSongs: number;

    constructor(init?: Partial<StatisticsModel>) {
        Object.assign(this, init);
    }
}
