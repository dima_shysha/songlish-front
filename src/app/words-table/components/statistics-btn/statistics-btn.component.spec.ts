import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsBtnComponent } from './statistics-btn.component';

describe('StatisticsBtnComponent', () => {
  let component: StatisticsBtnComponent;
  let fixture: ComponentFixture<StatisticsBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatisticsBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
