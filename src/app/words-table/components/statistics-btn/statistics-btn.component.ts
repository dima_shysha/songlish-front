import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { StatisticsModalComponent } from './statistics-modal/statistics-modal.component';

@Component({
    selector: 'sng-statistics-btn',
    templateUrl: './statistics-btn.component.html',
    styleUrls: ['./statistics-btn.component.scss'],
})
export class StatisticsBtnComponent {
    constructor(private dialog: MatDialog) {}

    onOpenStatistics(): void {
        this.dialog.open(StatisticsModalComponent);
    }
}
