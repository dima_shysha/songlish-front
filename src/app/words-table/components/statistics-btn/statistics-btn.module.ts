import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsBtnComponent } from './statistics-btn.component';
import { MatButtonModule } from '@angular/material/button';
import { StatisticsModalModule } from './statistics-modal/statistics-modal.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [StatisticsBtnComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        StatisticsModalModule,
        MatDialogModule,
    ],
    exports: [StatisticsBtnComponent],
})
export class StatisticsBtnModule {}
