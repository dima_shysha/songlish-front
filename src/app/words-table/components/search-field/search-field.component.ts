import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs';

@Component({
    selector: 'sng-search-field',
    templateUrl: './search-field.component.html',
    styleUrls: ['./search-field.component.scss'],
})
export class SearchFieldComponent implements OnInit {
    @Output() fieldInput = new EventEmitter<string>();

    fieldControl = new FormControl();

    ngOnInit(): void {
        this.fieldControl.valueChanges.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            tap(x => {
                this.fieldInput.emit(x);
            })
        );
    }

    onInput(event: any) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.fieldInput.emit(filterValue);
    }
}
