export class WordDetails {
    wordId: string;
    text: string;
    translations: string[];
    status: string;
}
