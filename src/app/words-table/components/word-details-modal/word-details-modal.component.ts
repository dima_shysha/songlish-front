import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component, Inject } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WarningDialogComponent } from 'src/app/_shared/warning-dialog/warning-dialog.component';
import { WordDetails } from './models/word-details.interface';

@Component({
    selector: 'sng-word-details-modal',
    templateUrl: './word-details-modal.component.html',
    styleUrls: ['./word-details-modal.component.scss'],
})
export class WordDetailsModalComponent {
    readonly separatorKeysCodes = [ENTER, COMMA] as const;

    model: WordDetails;

    constructor(
        public dialogRef: MatDialogRef<WarningDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { model: WordDetails }
    ) {
        this.model = data.model;
    }

    add(event: MatChipInputEvent): void {
        const value = (event.value || '').trim();

        if (value) {
            this.model.translations.push(value);
        }

        event.chipInput!.clear();
    }

    remove(translation: string): void {
        const index = this.model.translations.indexOf(translation);

        if (index >= 0) {
            this.model.translations.splice(index, 1);
        }
    }

    onSave() {
        this.dialogRef.close(this.model);
    }
}
