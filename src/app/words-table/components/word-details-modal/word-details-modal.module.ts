import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordDetailsModalComponent } from './word-details-modal.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';

@NgModule({
    declarations: [WordDetailsModalComponent],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatChipsModule,
        MatInputModule,
        MatDialogModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatIconModule,
        MatRippleModule,
    ],
    exports: [WordDetailsModalComponent],
})
export class WordDetailsModalModule {}
