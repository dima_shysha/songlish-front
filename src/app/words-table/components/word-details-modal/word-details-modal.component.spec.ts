import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WordDetailsModalComponent } from './word-details-modal.component';

describe('WordDetailsModalComponent', () => {
  let component: WordDetailsModalComponent;
  let fixture: ComponentFixture<WordDetailsModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WordDetailsModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WordDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
