import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { WordListItem } from './interfaces/word-list-item.interface';

@Component({
    selector: 'sng-word-list-tile[word]',
    templateUrl: './word-list-tile.component.html',
    styleUrls: ['./word-list-tile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WordListTileComponent {
    @Input()
    public get word(): WordListItem {
        return this._word;
    }
    public set word(v: WordListItem) {
        this._word = v;
        this.setStatus();
    }
    private _word: WordListItem;

    @Output() delete = new EventEmitter();

    @Output() changeStatus = new EventEmitter();

    isWordStudied: boolean;

    onDelete(): void {
        this.delete.emit();
    }

    onChangeStatus(event: MatCheckboxChange) {
        this.changeStatus.emit(
            event.checked ? WORD_STATUS.studied : WORD_STATUS.onStudy
        );
    }

    private setStatus(): void {
        this.isWordStudied = this.word.status === WORD_STATUS.studied;
    }
}
