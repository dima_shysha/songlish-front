import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordListTileComponent } from './word-list-tile.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRippleModule } from '@angular/material/core';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [WordListTileComponent],
    imports: [
        CommonModule,
        MatExpansionModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatRippleModule,
        FormsModule,
    ],
    exports: [WordListTileComponent],
})
export class WordListTileModule {}
