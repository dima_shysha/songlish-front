export class WordListItem {
    text: string;
    translations: string[];
    status: string;
}
