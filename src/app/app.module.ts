import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NgxsModule } from '@ngxs/store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { SongAddingPageModule } from './song-adding-page/song-adding-page.module';
import { RegistrationModule } from './auth/auth.module';
import { MatDividerModule } from '@angular/material/divider';
import { LoadingModule } from './_shared/loading/loading.module';
import { SnacksModule } from './_shared/snacks/snacks.module';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/interceptors/auth.interceptor';
import { SidenavService } from './_shared/sidenav/sidenav.service';
import { CardTrainingPageModule } from './trainings/card-training-page/card-training-page.module';
import { WordTranslateTrainingPageModule } from './trainings/word-translate-training-page/word-translate-training-page.module';
import { OptionsTrainingModule } from './trainings/options-training/options-training.module';
import { TrainingSelectionPageModule } from './training-selection-page/training-selection-page.module';
import { SongSearchPageModule } from './song-search-page/song-search-page.module';
import { PageHeaderModule } from './home/components/page-header/page-header.module';
import { HomeModule } from './home/home.module';
import { GapsTrainingPageModule } from './trainings/gaps-training/gaps-training-page.module';

@NgModule({
    declarations: [AppComponent],
    imports: [
        NgxsModule.forRoot(),
        NgxsRouterPluginModule.forRoot(),
        NgxsLoggerPluginModule.forRoot({
            disabled: environment.production,
        }),
        HomeModule,
        AppRoutingModule,
        PageHeaderModule,
        CardTrainingPageModule,
        OptionsTrainingModule,
        TrainingSelectionPageModule,
        SongAddingPageModule,
        SongSearchPageModule,
        WordTranslateTrainingPageModule,
        GapsTrainingPageModule,
        RegistrationModule,
        LoadingModule,
        SnacksModule,
        //
        BrowserModule,
        MatSidenavModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        SidenavService,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
