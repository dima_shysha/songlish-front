import { TestBed } from '@angular/core/testing';

import { OptionsTrainingResolver } from './options-training.resolver';

describe('OptionsTrainingResolver', () => {
  let resolver: OptionsTrainingResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(OptionsTrainingResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
