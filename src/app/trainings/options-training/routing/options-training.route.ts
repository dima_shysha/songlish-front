import { Route } from '@angular/router';
import { OptionsTrainingComponent } from '../options-training.component';
import { OptionsTrainingResolver } from './resolvers/options-training.resolver';

export const OPTIONS_TRAINING_ROUTE: Route = {
    path: 'options',
    component: OptionsTrainingComponent,
    resolve: { message: OptionsTrainingResolver },
};
