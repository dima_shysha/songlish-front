import { Component, EventEmitter, Input, Output } from '@angular/core';
import { OptionsTrainingModel } from '../../models/options-training.model';

@Component({
    selector: 'sng-answer-options[word]',
    templateUrl: './answer-options.component.html',
    styleUrls: ['./answer-options.component.scss'],
})
export class AnswerOptionsComponent {
    @Input() word: OptionsTrainingModel;

    @Output() answered = new EventEmitter();

    isAnswerShown = false;

    answeredIndex: number;

    options: string[];

    constructor() {}

    onOptionClick(index: number): void {
        this.viewAnswer(index).then(() => {
            this.answered.emit();
        });
    }

    private viewAnswer(answeredIndex: number): Promise<void> {
        return new Promise(resolve => {
            this.answeredIndex = answeredIndex;
            this.isAnswerShown = true;
            setTimeout(() => {
                this.isAnswerShown = false;
                this.answeredIndex = null;
                resolve();
            }, 2000);
        });
    }
}
