import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnswerOptionsComponent } from './answer-options.component';
import { MatButtonModule } from '@angular/material/button';
import { JoinPipeModule } from 'src/app/_shared/join-pipe/join-pipe.module';

@NgModule({
    declarations: [AnswerOptionsComponent],
    imports: [CommonModule, MatButtonModule, JoinPipeModule],
    exports: [AnswerOptionsComponent],
})
export class AnswerOptionsModule {}
