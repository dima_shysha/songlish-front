import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { UpdateWordDto } from 'src/app/trainings/card-training-page/state/services/dtos/update-word.dto';
import { ArrayHelper } from 'src/app/_shared/helpers/array.helper';
import { environment } from 'src/environments/environment';
import { OptionModel } from '../../models/option.model';
import { OptionsTrainingModel } from '../../models/options-training.model';
import { OptionsTrainingDto } from './dtos/options-training.dto';
import { ReversedOptionsTrainingDto } from './dtos/reversed-options-training.dto';

@Injectable()
export class OptionsTrainingService {
    private readonly apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getOptions(size: number): Observable<OptionsTrainingModel[]> {
        return this.http
            .get<OptionsTrainingDto[]>(
                `${this.apiUrl}/dictionary/training/select/${size}`
            )
            .pipe(
                map(items => {
                    return items.map(item => {
                        return new OptionsTrainingModel({
                            text: item.text,
                            wordId: item.wordId,
                            options: this.getPreparedOptions(
                                item.inCorrectTranslations,
                                item.translation
                            ),
                        });
                    });
                })
            );
    }

    getReversedOptions(size: number): Observable<OptionsTrainingModel[]> {
        return this.http
            .get<ReversedOptionsTrainingDto[]>(
                `${this.apiUrl}/dictionary/training/revert/select/${size}`
            )
            .pipe(
                map(items => {
                    return items.map(item => {
                        return new OptionsTrainingModel({
                            text: item.texts,
                            wordId: item.wordId,
                            options: this.getPreparedOptions(
                                item.inCorrectTranslations,
                                item.translation
                            ),
                        });
                    });
                })
            );
    }

    updateWords(words: UpdateWordDto[]): Observable<any> {
        return this.http.put(`${this.apiUrl}/dictionary`, words);
    }

    private getPreparedOptions(
        incorrect: string[],
        correct: string
    ): OptionModel[] {
        const result = [
            new OptionModel({ text: correct, isCorrect: true }),
            ...incorrect.map(
                x => new OptionModel({ text: x, isCorrect: false })
            ),
        ];

        return ArrayHelper.shuffle(result);
    }
}
