import { TestBed } from '@angular/core/testing';

import { OptionsTrainingService } from './options-training.service';

describe('OptionsTrainingService', () => {
  let service: OptionsTrainingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OptionsTrainingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
