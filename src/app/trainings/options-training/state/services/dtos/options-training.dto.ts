export class OptionsTrainingDto {
    text: string;
    inCorrectTranslations: string[];
    translation: string;
    wordId: string;

    constructor(init?: Partial<OptionsTrainingDto>) {
        Object.assign(this, init);
    }
}
