export class ReversedOptionsTrainingDto {
    texts: string;
    inCorrectTranslations: string[];
    translation: string;
    wordId: string;

    constructor(init?: Partial<ReversedOptionsTrainingDto>) {
        Object.assign(this, init);
    }
}
