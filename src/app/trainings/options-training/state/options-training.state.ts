import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { tap } from 'rxjs';
import { LoadingService } from 'src/app/_shared/loading/services/loading.service';
import { OpenSuccessSnack } from 'src/app/_shared/snacks/state/snack.actions';
import { OptionsTrainingExtModel } from '../models/options-training-ext.model';
import {
    EndTrainingAndSaveChanges,
    GetOptions,
    SetCurrentWordStatus,
    SetNextWord,
} from './options-training.actions';
import { UpdateWordDto } from './services/dtos/update-word.dto';
import { OptionsTrainingService } from './services/options-training.service';

export class OptionsTrainingStateModel {
    options: OptionsTrainingExtModel[];
    currentWordIndex: number;
}

const defaults = {
    options: [],
    currentWordIndex: null,
} as OptionsTrainingStateModel;

@State<OptionsTrainingStateModel>({
    name: 'optionsTraining',
    defaults,
})
@Injectable()
export class OptionsTrainingState {
    @Selector()
    static currentWord(
        state: OptionsTrainingStateModel
    ): OptionsTrainingExtModel {
        return state.options[state.currentWordIndex];
    }

    @Selector()
    static isLastWord(state: OptionsTrainingStateModel): boolean {
        return state.currentWordIndex === state.options.length - 1;
    }

    constructor(
        private optionsTrainingService: OptionsTrainingService,
        private loadingService: LoadingService
    ) {}

    @Action(GetOptions)
    onGetOptions(
        { patchState }: StateContext<OptionsTrainingStateModel>,
        { options }: GetOptions
    ) {
        if (options.reversed) {
            return this.optionsTrainingService
                .getReversedOptions(options.size)
                .pipe(
                    tap(options => {
                        patchState({
                            options,
                            currentWordIndex: 0,
                        });
                    })
                );
        }

        return this.optionsTrainingService.getOptions(options.size).pipe(
            tap(options => {
                patchState({
                    options,
                    currentWordIndex: 0,
                });
            })
        );
    }

    @Action(SetNextWord)
    onSetNextWord({
        patchState,
        getState,
    }: StateContext<OptionsTrainingStateModel>) {
        const { currentWordIndex } = getState();
        patchState({
            currentWordIndex: currentWordIndex + 1,
        });
    }

    @Action(SetCurrentWordStatus)
    onSetCurrentWordStatus(
        { setState, getState }: StateContext<OptionsTrainingStateModel>,
        { status }: SetCurrentWordStatus
    ) {
        const { currentWordIndex } = getState();
        setState(
            patch({
                options: updateItem<OptionsTrainingExtModel>(
                    currentWordIndex,
                    patch({ status })
                ),
            })
        );
    }

    @Action(EndTrainingAndSaveChanges)
    onEndTraining({
        setState,
        getState,
        dispatch,
    }: StateContext<OptionsTrainingStateModel>) {
        this.loadingService.open('Сохраняем ваши изменения');
        const { options } = getState();
        return this.optionsTrainingService
            .updateWords(
                options.map<UpdateWordDto>(x => {
                    return { wordId: x.wordId, wordStatus: x.status };
                })
            )
            .pipe(
                tap(() => {
                    dispatch(new Navigate(['search']));
                    setState(defaults);
                    this.loadingService.close();
                    dispatch(new OpenSuccessSnack('Тренировка закончена'));
                })
            );
    }
}
