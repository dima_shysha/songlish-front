import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';

export class GetOptions {
    static readonly type = '[OptionsTraining] Get options';

    constructor(public options: SequentialTrainingOptionsModel) {}
}

export class SetNextWord {
    static readonly type = '[OptionsTraining] Set next word';
}

export class SetCurrentWordStatus {
    static readonly type = '[OptionsTraining] Set current word status';

    constructor(public status: string) {}
}

export class EndTrainingAndSaveChanges {
    static readonly type = '[OptionsTraining] End training and save changes';

    constructor() {}
}
