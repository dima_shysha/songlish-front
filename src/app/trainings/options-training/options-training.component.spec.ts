import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionsTrainingComponent } from './options-training.component';

describe('OptionsTrainingComponent', () => {
  let component: OptionsTrainingComponent;
  let fixture: ComponentFixture<OptionsTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptionsTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionsTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
