import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptionsTrainingComponent } from './options-training.component';
import { OptionsTrainingService } from './state/services/options-training.service';
import { OptionsTrainingResolver } from './routing/resolvers/options-training.resolver';
import { NgxsModule } from '@ngxs/store';
import { OptionsTrainingState } from './state/options-training.state';
import { EndTrainingBtnModule } from '../_shared/end-training-btn/end-training-btn.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AnswerOptionsModule } from './components/answer-options/answer-options.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [OptionsTrainingComponent],
    providers: [OptionsTrainingService, OptionsTrainingResolver],
    imports: [
        CommonModule,
        NgxsModule.forFeature([OptionsTrainingState]),
        EndTrainingBtnModule,
        AnswerOptionsModule,
        MatCheckboxModule,
        FormsModule,
    ],
    exports: [OptionsTrainingComponent],
})
export class OptionsTrainingModule {}
