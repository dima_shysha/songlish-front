import { OptionModel } from './option.model';

export class OptionsTrainingModel {
    text: string;
    options: OptionModel[];
    wordId: string;

    constructor(init?: Partial<OptionsTrainingModel>) {
        Object.assign(this, init);
    }
}
