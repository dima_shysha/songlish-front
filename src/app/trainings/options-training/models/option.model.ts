export class OptionModel {
    text: string;
    isCorrect: boolean;

    constructor(init?: Partial<OptionModel>) {
        Object.assign(this, init);
    }
}
