import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { OptionsTrainingModel } from './options-training.model';

export class OptionsTrainingExtModel extends OptionsTrainingModel {
    status? = WORD_STATUS.onStudy;

    constructor(init?: Partial<OptionsTrainingExtModel>) {
        super();
        Object.assign(this, init);
    }
}
