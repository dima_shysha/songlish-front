import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { SubSink } from 'subsink';
import { OptionsTrainingExtModel } from './models/options-training-ext.model';
import {
    EndTrainingAndSaveChanges,
    SetCurrentWordStatus,
    SetNextWord,
} from './state/options-training.actions';
import { OptionsTrainingState } from './state/options-training.state';

@Component({
    selector: 'sng-options-training',
    templateUrl: './options-training.component.html',
    styleUrls: ['./options-training.component.scss'],
})
export class OptionsTrainingComponent implements OnInit, OnDestroy {
    @Select(OptionsTrainingState.currentWord)
    currentWord$: Observable<OptionsTrainingExtModel>;

    options: OptionsTrainingExtModel[];

    isWordStudied: boolean;

    constructor(private store: Store, private sub: SubSink) {}

    ngOnInit(): void {
        this.sub.add(
            this.currentWord$.subscribe(x => {
                this.isWordStudied = x?.status === WORD_STATUS.studied;
            })
        );
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    onAnswer(): void {
        const isLastWord = this.store.selectSnapshot(
            OptionsTrainingState.isLastWord
        );

        if (isLastWord) {
            this.onTrainingEnd();
        }

        this.store.dispatch(new SetNextWord());
    }

    onTrainingEnd() {
        this.store.dispatch(new EndTrainingAndSaveChanges());
    }

    onStatusChanged(event: any) {
        this.store.dispatch(
            new SetCurrentWordStatus(
                event.checked ? WORD_STATUS.studied : WORD_STATUS.onStudy
            )
        );
    }
}
