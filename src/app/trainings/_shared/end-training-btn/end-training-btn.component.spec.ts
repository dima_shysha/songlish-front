import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndTrainingBtnComponent } from './end-training-btn.component';

describe('EndTrainingBtnComponent', () => {
  let component: EndTrainingBtnComponent;
  let fixture: ComponentFixture<EndTrainingBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EndTrainingBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndTrainingBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
