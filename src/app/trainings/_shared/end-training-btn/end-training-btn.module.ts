import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EndTrainingBtnComponent } from './end-training-btn.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [EndTrainingBtnComponent],
    imports: [CommonModule, MatButtonModule],
    exports: [EndTrainingBtnComponent],
})
export class EndTrainingBtnModule {}
