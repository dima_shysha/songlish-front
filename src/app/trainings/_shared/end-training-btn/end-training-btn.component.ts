import { Component } from '@angular/core';

@Component({
  selector: 'sng-end-training-btn',
  templateUrl: './end-training-btn.component.html',
  styleUrls: ['./end-training-btn.component.scss']
})
export class EndTrainingBtnComponent {}
