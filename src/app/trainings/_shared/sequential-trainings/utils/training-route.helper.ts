import { ActivatedRouteSnapshot } from '@angular/router';
import { DEFAULT_TRAINING_SIZE } from '../constants/default-training-size.const';

export class TrainingRouteHelper {
    static getSize(route: ActivatedRouteSnapshot): number {
        const sizeParam = route.paramMap.get('size');
        if (!sizeParam) {
            return DEFAULT_TRAINING_SIZE;
        }

        const size = Number(sizeParam);
        if (!size) {
            return DEFAULT_TRAINING_SIZE;
        }

        return size;
    }

    static getReversed(route: ActivatedRouteSnapshot): boolean {
        return route.paramMap.get('reversed') === 'true';
    }
}
