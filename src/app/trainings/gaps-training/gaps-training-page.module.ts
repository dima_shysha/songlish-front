import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { MatMenuModule } from '@angular/material/menu';
import { EndTrainingBtnModule } from '../_shared/end-training-btn/end-training-btn.module';
import { LoadingSpinnerModule } from 'src/app/_shared/loading-directive/loading-spinner/loading-spinner.module';
import { YoutubePlayerModule } from 'src/app/_shared/youtube-player/youtube-player.module';
import { GapsTrainingPageComponent } from './gaps-training-page.component';
import { GapsTrainingResolver } from './routing/resolvers/gaps-training.resolver';
import { GapsTrainingState } from './state/gaps-training.state';
import { GapsTrainingApiService } from './state/services/gaps-training-api.service';

@NgModule({
    declarations: [GapsTrainingPageComponent],
    providers: [GapsTrainingResolver, GapsTrainingApiService],
    imports: [
        NgxsModule.forFeature([GapsTrainingState]),
        EndTrainingBtnModule,
        YoutubePlayerModule,
        LoadingSpinnerModule,
        CommonModule,
        MatMenuModule,
    ],
    exports: [GapsTrainingPageComponent],
})
export class GapsTrainingPageModule {}
