export class SongModel {
    authorName: string;
    songName: string;
    videoUri: string;

    constructor(init?: Partial<SongModel>) {
        Object.assign(this, init);
    }
}
