export class WordGapModel {
    id?: string;
    foreign: string;

    // extension
    isAnswered? = false;

    constructor(init?: Partial<WordGapModel>) {
        Object.assign(this, init);
    }
}
