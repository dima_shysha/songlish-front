import { SongModel } from './song.model';
import { WordGapModel } from './word-gap.model';
import { WordOptionModel } from './word-option.model';

export class TrainingDataModel {
    song: SongModel;
    textItems: WordGapModel[];
    options: WordOptionModel[];

    constructor(init?: Partial<TrainingDataModel>) {
        Object.assign(this, init);
    }
}
