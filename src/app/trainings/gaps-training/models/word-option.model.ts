export class WordOptionModel {
    id: string;
    foreign: string;

    // extention
    appearanceCount? = 1;

    constructor(init?: Partial<WordOptionModel>) {
        Object.assign(this, init);
    }
}
