import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { patch, removeItem, updateItem } from '@ngxs/store/operators';
import { Observable, tap } from 'rxjs';
import { updateItems } from 'src/app/_shared/ngxs-custom-operators/custom-operators';
import { OpenSuccessSnack } from 'src/app/_shared/snacks/state/snack.actions';
import { SongModel } from '../models/song.model';
import { TrainingDataModel } from '../models/training-data.model';
import { WordGapModel } from '../models/word-gap.model';
import { WordOptionModel } from '../models/word-option.model';
import {
    GetTrainingData,
    ResetGapsTraining,
    SelectOption,
    SetGap,
} from './gaps-training.actions';
import { GapsTrainingApiService } from './services/gaps-training-api.service';

export class GapsTrainingStateModel {
    public song: SongModel;
    public wordOptions: WordOptionModel[];
    public textItems: WordGapModel[];
    public selectedGap: WordGapModel;
}

const defaults: GapsTrainingStateModel = {
    song: null,
    wordOptions: [],
    textItems: [],
    selectedGap: null,
};

@State<GapsTrainingStateModel>({
    name: 'fillGapsTraining',
    defaults,
})
@Injectable()
export class GapsTrainingState {
    @Selector()
    static song(state: GapsTrainingStateModel): SongModel {
        return state.song;
    }

    @Selector()
    static wordOptions(state: GapsTrainingStateModel): WordOptionModel[] {
        return state.wordOptions;
    }

    @Selector()
    static textItems(state: GapsTrainingStateModel): WordGapModel[] {
        return state.textItems;
    }

    @Selector()
    static selectedGap(state: GapsTrainingStateModel): WordGapModel {
        return state.selectedGap;
    }

    constructor(private fillGapsTrainingApiService: GapsTrainingApiService) {}

    @Action(GetTrainingData)
    onGetTrainingData(
        { patchState }: StateContext<GapsTrainingStateModel>,
        { id }: GetTrainingData
    ): Observable<TrainingDataModel> {
        return this.fillGapsTrainingApiService.getTrainingData(id).pipe(
            tap(item => {
                patchState({
                    wordOptions: item.options,
                    textItems: item.textItems,
                    song: item.song,
                });
            })
        );
    }

    @Action(SetGap)
    onSetGap(
        { patchState }: StateContext<GapsTrainingStateModel>,
        { model }: SetGap
    ): void {
        patchState({
            selectedGap: model,
        });
    }

    @Action(SelectOption)
    onSelectOption(
        { getState, setState, dispatch }: StateContext<GapsTrainingStateModel>,
        { model }: SelectOption
    ): void {
        const { selectedGap, wordOptions } = getState();

        if (selectedGap.id === model.id) {
            setState(
                patch({
                    textItems: updateItems<WordGapModel>(
                        x => x.id === selectedGap.id,
                        patch<WordGapModel>({ isAnswered: true })
                    ),
                    wordOptions:
                        model.appearanceCount > 1
                            ? updateItem<WordOptionModel>(
                                  x => x.id === model.id,
                                  patch<WordOptionModel>({
                                      appearanceCount:
                                          model.appearanceCount - 1,
                                  })
                              )
                            : removeItem<WordOptionModel>(
                                  x => x.id === model.id
                              ),
                })
            );

            if (
                wordOptions.length === 1 &&
                wordOptions[0].appearanceCount === 1
            ) {
                dispatch([
                    new Navigate(['search']),
                    new OpenSuccessSnack('Тренировка закончена'),
                ]);
            }
        }
    }

    @Action(ResetGapsTraining)
    onReset({ setState }: StateContext<GapsTrainingStateModel>) {
        setState(defaults);
    }
}
