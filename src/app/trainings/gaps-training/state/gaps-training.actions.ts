import { WordGapModel } from '../models/word-gap.model';
import { WordOptionModel } from '../models/word-option.model';

export class GetTrainingData {
    static readonly type = '[GapsTraining] Get training data';

    constructor(public id: string) {}
}

export class SetGap {
    static readonly type = '[GapsTraining] Set gap';

    constructor(public model: WordGapModel) {}
}

export class SelectOption {
    static readonly type = '[GapsTraining] Select option';

    constructor(public model: WordOptionModel) {}
}

export class ResetGapsTraining {
    static readonly type = '[GapsTraining] Reset fill gaps training';
}
