import { SongModel } from 'src/app/song-adding-page/models/song.model';
import { WordGapModel } from '../../../models/word-gap.model';
import { WordOptionModel } from '../../../models/word-option.model';

export class GapsTrainingDataExtractor {
    constructor(
        private song: SongModel,
        private userWordsOnStudyIds: string[]
    ) {}

    getTextItems(): WordGapModel[] {
        return this.song.text.map(item => {
            if (
                !item.wordId ||
                !this.userWordsOnStudyIds.includes(item.wordId)
            ) {
                return new WordGapModel({
                    foreign: item.text,
                    id: null,
                    isAnswered: false,
                });
            }

            return new WordGapModel({
                foreign: item.text.trimStart(),
                id: item.wordId,
                isAnswered: false,
            });
        });
    }

    getOptions(): WordOptionModel[] {
        let options = this.song.text
            .filter(
                x => x.wordId && this.userWordsOnStudyIds.includes(x.wordId)
            )
            .map(item => {
                return new WordOptionModel({
                    appearanceCount: 1,
                    foreign: item.text.toLowerCase(),
                    id: item.wordId,
                });
            });
        options = options
            .filter(
                (value, index, self) =>
                    self.findIndex(x => x.id === value.id) === index
            )
            .sort((x1, x2) => x1.foreign.localeCompare(x2.foreign));

        return options;
    }
}
