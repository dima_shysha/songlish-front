import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, zip } from 'rxjs';
import * as song from 'src/app/song-adding-page/models/song.model';
import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { environment } from 'src/environments/environment';
import { SongModel } from '../../models/song.model';
import { TrainingDataModel } from '../../models/training-data.model';
import { GapsTrainingDataExtractor } from './training-data-extractor/gaps-training-data-extractor';

@Injectable()
export class GapsTrainingApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getTrainingData(songId: string): Observable<TrainingDataModel> {
        return zip(this.getSong(songId), this.getUserWordsIds()).pipe(
            map(value => {
                const songModel = value[0];
                const wordIds = value[1];
                const extractor = new GapsTrainingDataExtractor(
                    songModel,
                    wordIds
                );

                const textItems = extractor.getTextItems();
                let options = extractor.getOptions();
                const song = new SongModel({ ...songModel });

                return new TrainingDataModel({
                    song,
                    textItems,
                    options,
                });
            })
        );
    }

    private getSong(songId: string) {
        return this.http
            .get<song.SongModel>(`${this.url}/songs/${songId}`)
            .pipe(
                map(song => {
                    song.text.forEach(item => {
                        item.isInDictionary = false;
                    });

                    return song;
                })
            );
    }

    private getUserWordsIds(): Observable<string[]> {
        return this.http
            .get<{ wordId: string; status: string }[]>(
                `${this.url}/dictionary/words`
            )
            .pipe(
                map(x =>
                    x
                        .filter(x => x.status === WORD_STATUS.onStudy)
                        .map(x => x.wordId)
                )
            );
    }
}
