import { TestBed } from '@angular/core/testing';

import { GapsTrainingApiService } from './gaps-training-api.service';

describe('GapsTrainingApiService', () => {
    let service: GapsTrainingApiService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(GapsTrainingApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
