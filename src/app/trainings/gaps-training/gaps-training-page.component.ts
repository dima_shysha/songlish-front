import { Component, OnDestroy, OnInit } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ScreenHelper } from 'src/app/_shared/helpers/screen.helper';
import { SubSink } from 'subsink';
import { SongModel } from './models/song.model';
import { WordGapModel } from './models/word-gap.model';
import { WordOptionModel } from './models/word-option.model';
import {
    ResetGapsTraining,
    SelectOption,
    SetGap,
} from './state/gaps-training.actions';
import { GapsTrainingState } from './state/gaps-training.state';

@Component({
    selector: 'sng-gaps-training-page',
    templateUrl: './gaps-training-page.component.html',
    styleUrls: ['./gaps-training-page.component.scss'],
})
export class GapsTrainingPageComponent implements OnInit, OnDestroy {
    @Select(GapsTrainingState.wordOptions) wordOptions$: Observable<
        WordOptionModel[]
    >;

    @Select(GapsTrainingState.textItems) textItems$: Observable<WordGapModel[]>;

    @Select(GapsTrainingState.song) song$: Observable<SongModel>;

    @Select(GapsTrainingState.selectedGap)
    selectedGap$: Observable<WordGapModel>;

    protected sub = new SubSink();

    // TODO: try to remvoe
    selectedGapElement: EventTarget;

    selectedGap: WordGapModel;

    isMobile: boolean;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.sub.add(
            this.selectedGap$.subscribe(item => {
                this.selectedGap = item;
            }),
            ScreenHelper.isMobile$.subscribe(x => {
                this.isMobile = x;
            })
        );
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
        this.store.dispatch(new ResetGapsTraining());
    }

    onGapClick(word: WordGapModel, event: MouseEvent): void {
        this.selectedGapElement = event.target;
        this.store.dispatch(new SetGap(word));
    }

    onOptionSelected(word: WordOptionModel): void {
        if (!word) {
            return;
        }

        this.store.dispatch(new SelectOption(word));
    }

    onWordOptionHover(word: WordOptionModel): void {
        if (this.isMobile || !word) {
            return;
        }

        (this.selectedGapElement as any).innerText = word.foreign;
    }

    onWordOptionLeave(): void {
        if (this.selectedGap?.isAnswered) {
            return;
        }

        (this.selectedGapElement as any).innerText = '';
    }

    onTrainingEnd() {
        this.store.dispatch(new Navigate(['search']));
    }
}
