import { Route } from '@angular/router';
import { GapsTrainingPageComponent } from '../gaps-training-page.component';
import { GapsTrainingResolver } from './resolvers/gaps-training.resolver';

export const FILL_GAPS_TRAINING_ROUTE: Route = {
    path: 'gaps/:songId',
    resolve: { message: GapsTrainingResolver },
    component: GapsTrainingPageComponent,
};
