import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { GetTrainingData } from '../../state/gaps-training.actions';

@Injectable()
export class GapsTrainingResolver implements Resolve<boolean> {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
        if (!route.paramMap.get('songId')) {
            return of(false);
        }

        const songId = route.paramMap.get('songId');
        this.store.dispatch([new GetTrainingData(songId)]);
        return of(true);
    }
}
