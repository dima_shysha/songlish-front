import { TestBed } from '@angular/core/testing';

import { GapsTrainingResolver } from './gaps-training.resolver';

describe('GapsTrainingResolver', () => {
    let resolver: GapsTrainingResolver;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        resolver = TestBed.inject(GapsTrainingResolver);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });
});
