import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { WordCardModel } from './models/word-card.model';
import {
    ChangeWordStatus,
    EndTrainingAndSaveChanges,
    SetSongLines,
} from './state/card-training.actions';
import { CardTrainingState } from './state/card-training.state';

@Component({
    selector: 'sng-card-training-page',
    templateUrl: './card-training-page.component.html',
    styleUrls: ['./card-training-page.component.scss'],
})
export class CardTrainingPageComponent implements OnInit {
    @Select(CardTrainingState.cards) cards$: Observable<WordCardModel[]>;

    @Select(CardTrainingState.currentWord)
    currentWord$: Observable<WordCardModel>;

    isWordStudied: boolean;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.currentWord$.subscribe(x => {
            this.isWordStudied = x?.status === WORD_STATUS.studied;
        });
    }

    onCardIndexChanged(index?: number) {
        this.store.dispatch(new SetSongLines(index));
    }

    onStatusChanged(event: MatCheckboxChange): void {
        this.store.dispatch(
            new ChangeWordStatus(
                event.checked ? WORD_STATUS.studied : WORD_STATUS.onStudy
            )
        );
    }

    onReachEnd(): void {
        this.store.dispatch(new EndTrainingAndSaveChanges());
    }

    onTrainingEnd() {
        this.store.dispatch(new EndTrainingAndSaveChanges());
    }
}
