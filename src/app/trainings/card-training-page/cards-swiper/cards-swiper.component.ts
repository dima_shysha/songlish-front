import SwiperCore, { Navigation, Swiper, Virtual } from 'swiper';
import {
    Component,
    ViewEncapsulation,
    ViewChild,
    ElementRef,
    Input,
    Output,
    EventEmitter,
    HostListener,
} from '@angular/core';
import { SwiperCard } from './interfaces/swiper-card.interface';

SwiperCore.use([Navigation, Virtual]);

@Component({
    selector: 'sng-cards-swiper',
    templateUrl: './cards-swiper.component.html',
    styleUrls: ['./cards-swiper.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CardsSwiperComponent {
    @Input() words: SwiperCard[];

    @Output() indexChanged = new EventEmitter<number>();

    @Output() reachEnd = new EventEmitter();

    @ViewChild('swiper', { read: ElementRef }) swiper: ElementRef;

    @ViewChild('card') card: any;

    @HostListener('document:keyup', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        switch (event.key) {
            case 'ArrowLeft':
                this.swipePrevious();
                break;
            case 'ArrowRight':
                this.swipeNext();
                break;
            case ' ':
                this.onCardFlip();
                break;
        }
    }

    cardIsFlipped: boolean = false;

    onCardFlip(): void {
        if (this.cardIsFlipped) {
            if (
                (this.swiper.nativeElement.swiper as Swiper).realIndex ===
                this.words.length - 1
            ) {
                this.reachEnd.emit();

                return;
            }

            this.swipeNext();

            return;
        }

        this.cardIsFlipped = true;
    }

    onSlideChange(event: Swiper[]) {
        this.cardIsFlipped = false;

        this.indexChanged.emit(event[0].activeIndex);
    }

    private swipeNext(): void {
        this.cardIsFlipped = false;
        (this.swiper.nativeElement.swiper as Swiper).slideNext();
    }

    private swipePrevious(): void {
        this.cardIsFlipped = false;
        (this.swiper.nativeElement.swiper as Swiper).slidePrev();
    }
}
