import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsSwiperComponent } from './cards-swiper.component';
import { SwiperModule } from 'swiper/angular';
import { JoinPipeModule } from 'src/app/_shared/join-pipe/join-pipe.module';

@NgModule({
    declarations: [CardsSwiperComponent],
    imports: [CommonModule, SwiperModule, JoinPipeModule],
    exports: [CardsSwiperComponent],
})
export class CardsSwiperModule {}
