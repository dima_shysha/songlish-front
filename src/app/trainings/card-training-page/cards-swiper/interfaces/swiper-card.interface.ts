export interface SwiperCard {
    front: string[] | string;
    back: string[] | string;
}
