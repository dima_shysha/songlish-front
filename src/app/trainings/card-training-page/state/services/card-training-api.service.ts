import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';
import { environment } from 'src/environments/environment';
import { CardDataModel } from '../../models/card-data.model';
import { CardTrainingDataModel } from '../../models/card-training-data.model';
import { WordCardModel } from '../../models/word-card.model';
import { UpdateWordDto } from './dtos/update-word.dto';
import { WordCardDto } from './dtos/word-card-dto';

@Injectable({
    providedIn: 'root',
})
export class CardTrainingApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getTrainingData(
        options: SequentialTrainingOptionsModel
    ): Observable<CardTrainingDataModel> {
        return this.http
            .get<WordCardDto[]>(
                `${this.url}/dictionary/training/${options.size}`
            )
            .pipe(
                map<WordCardDto[], CardTrainingDataModel>(items => {
                    return new CardTrainingDataModel({
                        cardDatas: items.map<CardDataModel>(
                            (x: WordCardDto) => {
                                return new CardDataModel({
                                    card: new WordCardModel({ ...x }),
                                });
                            }
                        ),
                    });
                })
            );
    }

    updateWords(words: UpdateWordDto[]): Observable<any> {
        return this.http.put(`${this.url}/dictionary`, words);
    }
}
