export class WordCardDto {
    word: {
        wordId: string;
        text: string;
        translations: [
            {
                wordId: string;
                text: string;
            }
        ];
    };
    status: string;
    translations: string[];
}
