import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { tap } from 'rxjs';
import { LoadingService } from 'src/app/_shared/loading/services/loading.service';
import { OpenSuccessSnack } from 'src/app/_shared/snacks/state/snack.actions';
import { SwiperCard } from '../cards-swiper/interfaces/swiper-card.interface';
import { CardDataModel } from '../models/card-data.model';
import { CardTrainingDataModel } from '../models/card-training-data.model';
import { WordCardModel } from '../models/word-card.model';
import {
    ChangeWordStatus,
    EndTrainingAndSaveChanges as EndTrainingAndSaveChanges,
    GetTrainingData,
    SetSongLines,
} from './card-training.actions';
import { CardTrainingApiService } from './services/card-training-api.service';
import { UpdateWordDto } from './services/dtos/update-word.dto';

export class CardTrainingStateModel {
    public cardDatas: CardDataModel[];
    public cards: SwiperCard[];
    public currentWord: WordCardModel;
}

const defaults: CardTrainingStateModel = {
    cardDatas: [],
    cards: [],
    currentWord: null,
};

@State<CardTrainingStateModel>({
    name: 'cardTraining',
    defaults,
})
@Injectable()
export class CardTrainingState {
    @Selector()
    static cardDatas(state: CardTrainingStateModel): CardDataModel[] {
        return state.cardDatas;
    }

    @Selector()
    static cards(state: CardTrainingStateModel): SwiperCard[] {
        return state.cards;
    }

    @Selector()
    static currentWord(state: CardTrainingStateModel): WordCardModel {
        return state.currentWord;
    }

    constructor(
        private cardTrainingApiService: CardTrainingApiService,
        private loadingService: LoadingService
    ) {}

    @Action(GetTrainingData)
    onGetTrainingData(
        { setState }: StateContext<CardTrainingStateModel>,
        { options }: GetTrainingData
    ) {
        return this.cardTrainingApiService.getTrainingData(options).pipe(
            tap(item => {
                setState({
                    cardDatas: item.cardDatas,
                    cards: options.reversed
                        ? this.extractRevertSwiperCards(item)
                        : this.extractSwiperCards(item),
                    currentWord: item.cardDatas[0].card,
                });
            })
        );
    }

    @Action(SetSongLines)
    onSetSongLines(
        { getState, patchState }: StateContext<CardTrainingStateModel>,
        { cardIndex }: SetSongLines
    ) {
        const { cardDatas } = getState();

        if (!cardDatas) {
            return;
        }

        patchState({
            currentWord: cardDatas[cardIndex].card,
        });
    }

    @Action(ChangeWordStatus)
    onChangeWordStatus(
        { setState, getState }: StateContext<CardTrainingStateModel>,
        { wordStatus }: ChangeWordStatus
    ) {
        const { currentWord } = getState();

        setState(
            patch({
                cardDatas: updateItem<CardDataModel>(
                    x => x.card.wordId == currentWord.wordId,
                    patch({
                        card: { ...currentWord, status: wordStatus },
                    })
                ),
            })
        );
    }

    @Action(EndTrainingAndSaveChanges)
    onEndTraining({
        setState,
        getState,
        dispatch,
    }: StateContext<CardTrainingStateModel>) {
        this.loadingService.open('Сохраняем ваши изменения');
        const { cardDatas } = getState();
        return this.cardTrainingApiService
            .updateWords(
                cardDatas
                    .map(x => x.card)
                    .map<UpdateWordDto>(x => {
                        return { wordId: x.wordId, wordStatus: x.status };
                    })
            )
            .pipe(
                tap(() => {
                    dispatch(new Navigate(['search']));

                    setState({
                        cardDatas: [],
                        cards: [],
                        currentWord: null,
                    });

                    this.loadingService.close();
                    dispatch(new OpenSuccessSnack('Тренировка закончена'));
                })
            );
    }

    private extractSwiperCards(item: CardTrainingDataModel): SwiperCard[] {
        return item.cardDatas.map(x => {
            return {
                front: x.card.text,
                back: x.card.translations,
            } as SwiperCard;
        });
    }

    private extractRevertSwiperCards(
        item: CardTrainingDataModel
    ): SwiperCard[] {
        return item.cardDatas.map(x => {
            return {
                front: x.card.translations,
                back: x.card.text,
            } as SwiperCard;
        });
    }
}
