import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';

export class GetTrainingData {
    static readonly type = '[CardTraining] Get training data';

    constructor(public options: SequentialTrainingOptionsModel) {}
}

export class SetSongLines {
    static readonly type = '[CardTraining] Set song lines';

    constructor(public cardIndex: number) {}
}

export class SetCurrentWord {
    static readonly type = '[CardTraining] Set current word';

    constructor(public cardIndex: number) {}
}

export class ChangeWordStatus {
    static readonly type = '[CardTraining] Change word knowledge level';

    constructor(public wordStatus: string) {}
}

export class EndTrainingAndSaveChanges {
    static readonly type = '[CardTraining] End training and save changes';
}
