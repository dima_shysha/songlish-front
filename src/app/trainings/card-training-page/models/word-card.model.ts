export class WordCardModel {
    wordId: string;
    text: string;
    translations: string[];
    status: string;

    constructor(init?: Partial<WordCardModel>) {
        Object.assign(this, init);
    }
}
