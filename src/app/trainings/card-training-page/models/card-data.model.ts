import { WordCardModel } from './word-card.model';

export class CardDataModel {
    card: WordCardModel;

    constructor(init?: Partial<CardDataModel>) {
        Object.assign(this, init);
    }
}
