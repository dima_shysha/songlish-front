import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { CardTrainingPageComponent } from './card-training-page.component';
import { NgxsModule } from '@ngxs/store';
import { CardTrainingState } from './state/card-training.state';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { CardsSwiperModule } from './cards-swiper/cards-swiper.module';
import { EndTrainingBtnModule } from '../_shared/end-training-btn/end-training-btn.module';
import { CardTrainingResolver } from './routing/resolvers/card-training.resolver';

@NgModule({
    declarations: [CardTrainingPageComponent],
    providers: [CardTrainingResolver],
    imports: [
        CardsSwiperModule,
        EndTrainingBtnModule,
        //
        NgxsModule.forFeature([CardTrainingState]),
        CommonModule,
        MatButtonModule,
        MatCheckboxModule,
        FormsModule,
    ],
})
export class CardTrainingPageModule {}
