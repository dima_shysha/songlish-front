import { Route } from '@angular/router';
import { CardTrainingPageComponent } from '../card-training-page.component';
import { CardTrainingResolver } from './resolvers/card-training.resolver';

export const CARD_TRAINING_ROUTE: Route = {
    path: 'cards',
    component: CardTrainingPageComponent,
    resolve: { message: CardTrainingResolver },
};
