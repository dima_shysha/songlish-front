import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';
import { TrainingRouteHelper } from 'src/app/trainings/_shared/sequential-trainings/utils/training-route.helper';
import { GetTrainingData } from '../../state/card-training.actions';

@Injectable()
export class CardTrainingResolver implements Resolve<boolean> {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
        const size = TrainingRouteHelper.getSize(route);
        const reversed = TrainingRouteHelper.getReversed(route);

        return this.store.dispatch(
            new GetTrainingData(
                new SequentialTrainingOptionsModel({ size, reversed })
            )
        );
    }
}
