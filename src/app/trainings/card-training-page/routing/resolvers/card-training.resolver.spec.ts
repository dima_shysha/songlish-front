import { TestBed } from '@angular/core/testing';

import { CardTrainingResolver } from './card-training.resolver';

describe('CardTrainingResolver', () => {
  let resolver: CardTrainingResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(CardTrainingResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
