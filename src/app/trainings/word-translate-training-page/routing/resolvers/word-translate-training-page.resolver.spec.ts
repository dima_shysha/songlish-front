import { TestBed } from '@angular/core/testing';

import { WordTranslateTrainingResolver } from './word-translate-training.resolver';

describe('WordTranslateTrainingPageResolver', () => {
    let resolver: WordTranslateTrainingResolver;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        resolver = TestBed.inject(WordTranslateTrainingResolver);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });
});
