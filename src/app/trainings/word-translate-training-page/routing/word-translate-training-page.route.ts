import { Route } from '@angular/router';
import { WordTranslateTrainingPageComponent } from '../word-translate-training-page.component';
import { WordTranslateTrainingResolver } from './resolvers/word-translate-training.resolver';

export const WORD_TRANSLATE_TRAINING_PAGE: Route = {
    path: 'word-translate',
    resolve: { message: WordTranslateTrainingResolver },
    component: WordTranslateTrainingPageComponent,
};
