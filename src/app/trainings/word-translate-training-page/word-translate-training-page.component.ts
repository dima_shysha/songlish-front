import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { WORD_STATUS } from 'src/app/_shared/constants/word-satatus.const';
import { WordModel } from './models/word.model';
import {
    ChangeWordStatus,
    EndTraining,
} from './state/word-translate-training.actions';
import { WordTranslateTrainingState } from './state/word-translate-training.state';

@Component({
    selector: 'sng-word-translate-training-page',
    templateUrl: './word-translate-training-page.component.html',
    styleUrls: ['./word-translate-training-page.component.scss'],
})
export class WordTranslateTrainingPageComponent implements OnInit {
    @Select(WordTranslateTrainingState.words) words$: Observable<WordModel[]>;

    @Select(WordTranslateTrainingState.currentWord)
    currentWord$: Observable<WordModel>;

    isWordStudied: boolean;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.currentWord$.subscribe(x => {
            this.isWordStudied = x?.status === WORD_STATUS.studied;
        });
    }

    onAnsweredCorrectly(_: any) {
        //
    }

    onStatusChanged(event: MatCheckboxChange): void {
        this.store.dispatch(
            new ChangeWordStatus(
                event.checked ? WORD_STATUS.studied : WORD_STATUS.onStudy
            )
        );
    }

    onTrainingEnd() {
        this.store.dispatch(new EndTraining());
    }

    onReachEnd(): void {
        this.store.dispatch(new EndTraining());
    }
}
