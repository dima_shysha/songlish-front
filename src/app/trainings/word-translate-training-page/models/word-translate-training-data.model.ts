import { WordDataModel } from './word-data.model';

export class WordTranslateTrainingDataModel {
    wordsData: WordDataModel[];

    constructor(init?: Partial<WordTranslateTrainingDataModel>) {
        Object.assign(this, init);
    }
}
