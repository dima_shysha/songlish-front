import { WordModel } from './word.model';

export class WordDataModel {
    word: WordModel;

    constructor(init?: Partial<WordDataModel>) {
        Object.assign(this, init);
    }
}
