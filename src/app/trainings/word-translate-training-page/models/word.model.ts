export class WordModel {
    wordId: string;
    text: string;
    translations: string[];
    status: string;

    constructor(init?: Partial<WordModel>) {
        Object.assign(this, init);
    }
}
