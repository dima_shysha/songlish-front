export interface QuestionItem {
    text: string[];
    translations: string[];
}
