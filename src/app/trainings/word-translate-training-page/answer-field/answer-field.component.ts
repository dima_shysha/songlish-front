import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    Output,
    ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { FieldState } from './enums/field-state.enum';
import { QuestionItem } from './interfaces/question-item.interface';

@Component({
    selector: 'sng-answer-field[words]',
    templateUrl: './answer-field.component.html',
    styleUrls: ['./answer-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnswerFieldComponent implements AfterViewInit {
    @Input() words: QuestionItem[];

    @Output() answeredCorrectly = new EventEmitter<number>();

    @Output() reachEnd = new EventEmitter();

    @ViewChild('input') input: ElementRef;

    control = new FormControl();

    currentIndex = 0;

    fieldState: FieldState;

    FieldState = FieldState;

    currentWordTranslates: string[] = [];

    disableInput = false;

    ngAfterViewInit(): void {
        this.input.nativeElement.focus();
    }

    onInput(value: string): void {
        if (this.isAnswerCorrect(value)) {
            this.disableInput = true;
            this.fieldState = FieldState.CORRECT;
            this.setCurrentWordTranslates();

            setTimeout(() => {
                if (this.currentIndex === this.words.length - 1) {
                    this.reachEnd.emit();
                    return;
                }

                this.answeredCorrectly.emit(++this.currentIndex);
                this.input.nativeElement.value = '';
                this.fieldState = FieldState.NEUTRAL;
                this.disableInput = false;
                this.resetCurrentWordTranslates();
            }, 1500);
        }
    }

    onHelp() {
        if (this.disableInput) {
            return;
        }

        this.setCurrentWordTranslates();
        setTimeout(() => {
            if (this.currentIndex === this.words.length - 1) {
                this.reachEnd.emit();
                return;
            }

            this.disableInput = true;
            this.answeredCorrectly.emit(++this.currentIndex);
            this.input.nativeElement.value = '';
            this.disableInput = false;
            this.resetCurrentWordTranslates();
        }, 1500);
    }

    private isAnswerCorrect(value: string) {
        const translations: string[] =
            this.words[this.currentIndex].translations;

        return translations.some(x => x.toLowerCase() === value.toLowerCase());
    }

    private setCurrentWordTranslates(): void {
        this.currentWordTranslates = this.words[this.currentIndex].translations;
    }

    private resetCurrentWordTranslates(): void {
        this.currentWordTranslates = [];
    }
}
