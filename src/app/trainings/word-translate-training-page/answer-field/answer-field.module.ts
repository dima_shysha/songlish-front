import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnswerFieldComponent } from './answer-field.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { JoinPipeModule } from 'src/app/_shared/join-pipe/join-pipe.module';

@NgModule({
    declarations: [AnswerFieldComponent],
    imports: [
        JoinPipeModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
    ],
    exports: [AnswerFieldComponent],
})
export class AnswerFieldModule {}
