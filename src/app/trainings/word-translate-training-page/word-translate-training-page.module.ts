import { NgModule } from '@angular/core';
import { WordTranslateTrainingPageComponent } from './word-translate-training-page.component';
import { CommonModule } from '@angular/common';
import { AnswerFieldModule } from './answer-field/answer-field.module';
import { NgxsModule } from '@ngxs/store';
import { WordTranslateTrainingState } from './state/word-translate-training.state';
import { WordTranslateTrainingApiService } from './state/services/word-translate-training-api.service';
import { WordTranslateTrainingResolver } from './routing/resolvers/word-translate-training.resolver';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { EndTrainingBtnModule } from '../_shared/end-training-btn/end-training-btn.module';

@NgModule({
    declarations: [WordTranslateTrainingPageComponent],
    providers: [WordTranslateTrainingApiService, WordTranslateTrainingResolver],
    imports: [
        NgxsModule.forFeature([WordTranslateTrainingState]),
        AnswerFieldModule,
        EndTrainingBtnModule,
        //
        CommonModule,
        MatCheckboxModule,
        FormsModule,
    ],
    exports: [WordTranslateTrainingPageComponent],
})
export class WordTranslateTrainingPageModule {}
