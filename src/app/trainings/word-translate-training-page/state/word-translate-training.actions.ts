import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';

export class GetTrainingData {
    static readonly type = '[WordTranslateTraining] Get training data';

    constructor(public options: SequentialTrainingOptionsModel) {}
}

export class SetCurrentWord {
    static readonly type = '[WordTranslateTraining] Set current word';

    constructor(public wordIndex: number) {}
}

export class ChangeWordStatus {
    static readonly type =
        '[WordTranslateTraining] Change word knowledge level';

    constructor(public level: string) {}
}

export class EndTraining {
    static readonly type = '[WordTranslateTraining] End training';
}
