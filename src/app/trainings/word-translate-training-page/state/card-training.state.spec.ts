import { TestBed, async } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { WordTranslateTrainingState } from './word-translate-training.state';
import { CardTrainingAction } from './word-translate-training.actions';

describe('CardTraining actions', () => {
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([WordTranslateTrainingState])],
        }).compileComponents();
        store = TestBed.get(Store);
    }));

    it('should create an action and add an item', () => {
        store.dispatch(new CardTrainingAction('item-1'));
        store
            .select(state => state.cardTraining.items)
            .subscribe((items: string[]) => {
                expect(items).toEqual(jasmine.objectContaining(['item-1']));
            });
    });
});
