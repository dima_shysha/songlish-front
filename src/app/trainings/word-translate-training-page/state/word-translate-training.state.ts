import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { tap } from 'rxjs';
import { OpenSuccessSnack } from 'src/app/_shared/snacks/state/snack.actions';
import { WordDataModel } from '../models/word-data.model';
import { WordModel } from '../models/word.model';
import {
    ChangeWordStatus,
    EndTraining,
    GetTrainingData,
} from './word-translate-training.actions';
import { WordTranslateTrainingApiService } from './services/word-translate-training-api.service';
import { WordTranslateTrainingDataModel } from '../models/word-translate-training-data.model';
import { UpdateWordDto } from './services/dtos/update-word.dto';
import { LoadingService } from 'src/app/_shared/loading/services/loading.service';
import { QuestionItem } from '../answer-field/interfaces/question-item.interface';

export class WordTranslateTrainingStateModel {
    public wordsData: WordDataModel[];
    public words: QuestionItem[];
    public currentWord: WordModel;
}

const defaults: WordTranslateTrainingStateModel = {
    wordsData: [],
    words: [],
    currentWord: null,
};

@State<WordTranslateTrainingStateModel>({
    name: 'wordTranslateTraining',
    defaults,
})
@Injectable()
export class WordTranslateTrainingState {
    constructor(
        private wortTranslateTrainingApiService: WordTranslateTrainingApiService,
        private loadingService: LoadingService
    ) {}

    @Selector()
    static wordsData(state: WordTranslateTrainingStateModel): WordDataModel[] {
        return state.wordsData;
    }

    @Selector()
    static words(state: WordTranslateTrainingStateModel): QuestionItem[] {
        return state.words;
    }

    @Selector()
    static currentWord(state: WordTranslateTrainingStateModel): WordModel {
        return state.currentWord;
    }

    @Action(GetTrainingData)
    onGetTrainingData(
        { setState }: StateContext<WordTranslateTrainingStateModel>,
        { options }: GetTrainingData
    ) {
        return this.wortTranslateTrainingApiService
            .getTrainingData(options)
            .pipe(
                tap((item: WordTranslateTrainingDataModel) => {
                    setState({
                        wordsData: item.wordsData,
                        words: options.reversed
                            ? this.extractReversedWords(item)
                            : this.extractWords(item),
                        currentWord: item.wordsData[0].word,
                    });
                })
            );
    }

    @Action(ChangeWordStatus)
    onChangeWordStatus(
        { setState, getState }: StateContext<WordTranslateTrainingStateModel>,
        { level }: ChangeWordStatus
    ) {
        const { currentWord } = getState();

        setState(
            patch({
                wordsData: updateItem<WordDataModel>(
                    x => x.word.text == currentWord.text,
                    patch({
                        word: { ...currentWord, status: level },
                    })
                ),
            })
        );
    }

    @Action(EndTraining)
    onEndTraining({
        setState,
        getState,
        dispatch,
    }: StateContext<WordTranslateTrainingStateModel>) {
        this.loadingService.open('Сохраняем ваши изменения');
        const { wordsData } = getState();
        return this.wortTranslateTrainingApiService
            .updateWords(
                wordsData
                    .map(x => x.word)
                    .map<UpdateWordDto>(x => {
                        return { wordId: x.wordId, wordStatus: x.status };
                    })
            )
            .pipe(
                tap(() => {
                    dispatch(new Navigate(['search']));

                    setState({
                        wordsData: [],
                        words: [],
                        currentWord: null,
                    });

                    this.loadingService.close();
                    dispatch(new OpenSuccessSnack('Тренировка закончена'));
                })
            );
    }

    private extractWords(
        trainingData: WordTranslateTrainingDataModel
    ): QuestionItem[] {
        return trainingData.wordsData.map(
            x =>
                <QuestionItem>{
                    text: [x.word.text],
                    translations: x.word.translations,
                }
        );
    }

    private extractReversedWords(
        trainingData: WordTranslateTrainingDataModel
    ): QuestionItem[] {
        return trainingData.wordsData.map(x => {
            return <QuestionItem>{
                text: x.word.translations,
                translations: [x.word.text],
            };
        });
    }
}
