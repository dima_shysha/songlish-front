import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';
import { environment } from 'src/environments/environment';
import { WordDataModel } from '../../models/word-data.model';
import { WordTranslateTrainingDataModel } from '../../models/word-translate-training-data.model';
import { WordModel } from '../../models/word.model';
import { UpdateWordDto } from './dtos/update-word.dto';

@Injectable()
export class WordTranslateTrainingApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getTrainingData(
        options: SequentialTrainingOptionsModel
    ): Observable<WordTranslateTrainingDataModel> {
        return this.http
            .get<WordModel[]>(`${this.url}/dictionary/training/${options.size}`)
            .pipe(
                map<WordModel[], WordTranslateTrainingDataModel>(items => {
                    return new WordTranslateTrainingDataModel({
                        wordsData: items.map<WordDataModel>(x => {
                            return new WordDataModel({
                                word: new WordModel({ ...x }),
                            });
                        }),
                    });
                })
            );
    }

    updateWords(words: UpdateWordDto[]): Observable<any> {
        return this.http.put(`${this.url}/dictionary`, words);
    }
}
