import { TestBed } from '@angular/core/testing';

import { WordTranslateTrainingApiService } from './word-translate-training-api.service';

describe('CardTrainingApiService', () => {
    let service: WordTranslateTrainingApiService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(WordTranslateTrainingApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
