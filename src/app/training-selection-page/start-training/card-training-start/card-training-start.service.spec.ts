import { TestBed } from '@angular/core/testing';

import { CardTrainingStartService } from './card-training-start.service';

describe('CardTrainingStartService', () => {
  let service: CardTrainingStartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardTrainingStartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
