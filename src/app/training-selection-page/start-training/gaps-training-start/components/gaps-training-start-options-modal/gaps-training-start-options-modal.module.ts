import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GapsTrainingStartOptionsModalComponent } from './gaps-training-start-options-modal.component';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { LoadingSpinnerModule } from 'src/app/_shared/loading-directive/loading-spinner/loading-spinner.module';
import { SubSink } from 'subsink';
import { SongsApiService } from './services/songs-api.service';

@NgModule({
    declarations: [GapsTrainingStartOptionsModalComponent],
    providers: [SongsApiService, SubSink],
    imports: [
        CommonModule,
        MatButtonModule,
        MatSelectModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatDialogModule,
        LoadingSpinnerModule,
    ],
    exports: [GapsTrainingStartOptionsModalComponent],
})
export class GapsTrainingStartOptionsModalModule {}
