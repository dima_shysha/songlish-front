import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { SequentialTrainingOptionsModalComponent } from 'src/app/training-selection-page/components/sequential-training-options-modal/sequential-training-options-modal.component';
import { SubSink } from 'subsink';
import { GapsTrainingOptionsModel } from '../../models/gaps-training-options.model';
import { SongListItemModel } from './services/models/song-list-item.model';
import { SongsApiService } from './services/songs-api.service';

@Component({
    selector: 'sng-gaps-training-start-options-modal',
    templateUrl: './gaps-training-start-options-modal.component.html',
    styleUrls: ['./gaps-training-start-options-modal.component.scss'],
})
export class GapsTrainingStartOptionsModalComponent implements OnInit {
    form: FormGroup;

    songs: SongListItemModel[];

    isLoading = true;

    noSongsToSelect = false;

    constructor(
        public dialogRef: MatDialogRef<SequentialTrainingOptionsModalComponent>,
        private songsService: SongsApiService,
        private store: Store,
        private sub: SubSink
    ) {}

    ngOnInit(): void {
        this.form = new FormGroup({
            songId: new FormControl('', [Validators.required]),
        });
        this.sub.add(
            this.songsService.getAll().subscribe(x => {
                if (!x.length) {
                    this.noSongsToSelect = true;
                    this.isLoading = false;
                    return;
                }

                this.songs = x;
                this.isLoading = false;
            })
        );
    }

    onClose(): void {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
            return;
        }

        const formModel = this.form.getRawValue();
        const options = new GapsTrainingOptionsModel({
            songId: formModel.songId,
        });
        this.dialogRef.close(options);
    }

    onGoToSongAdding(): void {
        this.store.dispatch(new Navigate(['song-search']));
        this.dialogRef.close();
    }
}
