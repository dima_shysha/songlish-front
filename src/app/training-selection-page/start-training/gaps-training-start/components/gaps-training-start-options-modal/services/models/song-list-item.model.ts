export class SongListItemModel {
    songId: string;
    authorName: string;
    songName: string;

    constructor(init?: Partial<SongListItemModel>) {
        Object.assign(this, init);
    }
}
