import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SongListItemModel } from './models/song-list-item.model';

@Injectable()
export class SongsApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getAll(): Observable<SongListItemModel[]> {
        return this.http
            .get<SongListItemModel[]>(`${this.url}/playlist`)
            .pipe(map(x => this.sortSongs(x)));
    }

    private sortSongs(songs: SongListItemModel[]) {
        return songs.sort((x1, x2) =>
            this.getFullSongName(x1).localeCompare(this.getFullSongName(x2))
        );
    }

    private getFullSongName(song: SongListItemModel) {
        return `${song.authorName} ${song.songName}`;
    }
}
