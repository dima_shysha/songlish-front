import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GapsTrainingStartOptionsModalComponent } from './gaps-training-start-options-modal.component';

describe('GapsTrainingStartOptionsModalComponent', () => {
    let component: GapsTrainingStartOptionsModalComponent;
    let fixture: ComponentFixture<GapsTrainingStartOptionsModalComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GapsTrainingStartOptionsModalComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(
            GapsTrainingStartOptionsModalComponent
        );
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
