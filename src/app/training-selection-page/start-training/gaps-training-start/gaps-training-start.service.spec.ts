import { TestBed } from '@angular/core/testing';

import { GapsTrainingStartService } from './gaps-training-start.service';

describe('GapsTrainingStartService', () => {
    let service: GapsTrainingStartService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(GapsTrainingStartService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
