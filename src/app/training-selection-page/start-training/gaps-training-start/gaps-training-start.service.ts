import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { TrainingStartService } from '../_factory/interface/training-start.service.interface';
import { GapsTrainingStartOptionsModalComponent } from './components/gaps-training-start-options-modal/gaps-training-start-options-modal.component';
import { GapsTrainingOptionsModel } from './models/gaps-training-options.model';

@Injectable()
export class GapsTrainingStartService implements TrainingStartService {
    constructor(private dialog: MatDialog, private store: Store) {}

    handle(): void {
        this.dialog
            .open<
                GapsTrainingStartOptionsModalComponent,
                any,
                GapsTrainingOptionsModel
            >(GapsTrainingStartOptionsModalComponent)
            .afterClosed()
            .subscribe(options => {
                if (!options) {
                    return;
                }

                this.store.dispatch(
                    new Navigate(['training', 'gaps', options.songId])
                );
            });
    }
}
