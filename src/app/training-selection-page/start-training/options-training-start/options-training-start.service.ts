import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';
import { SequentialTrainingOptionsModalComponent } from '../../components/sequential-training-options-modal/sequential-training-options-modal.component';
import { TrainingStartService } from '../_factory/interface/training-start.service.interface';
import { SequentialTrainingOptionsModalData } from '../_shared/components/interface/sequential-training-options-modal-data';

@Injectable()
export class OptionsTrainingStartService implements TrainingStartService {
    constructor(private store: Store, private dialog: MatDialog) {}
    handle(): void {
        this.dialog
            .open<
                SequentialTrainingOptionsModalComponent,
                any,
                SequentialTrainingOptionsModel
            >(SequentialTrainingOptionsModalComponent, {
                data: {
                    title: 'Варианты ответов',
                } as SequentialTrainingOptionsModalData,
            })
            .afterClosed()
            .subscribe(options => {
                if (!options) {
                    return;
                }

                this.store.dispatch(
                    new Navigate(['training', 'options', options])
                );
            });
    }
}
