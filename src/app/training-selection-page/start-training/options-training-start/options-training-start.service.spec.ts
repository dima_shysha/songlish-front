import { TestBed } from '@angular/core/testing';

import { OptionsTrainingStartService } from './options-training-start.service';

describe('OptionsTrainingStartService', () => {
  let service: OptionsTrainingStartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OptionsTrainingStartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
