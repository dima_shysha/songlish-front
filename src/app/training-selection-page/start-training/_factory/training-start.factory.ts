import { Injectable, Injector } from '@angular/core';
import { TrainingMode } from '../../models/enums/training-mode.enum';
import { CardTrainingStartService } from '../card-training-start/card-training-start.service';
import { GapsTrainingStartService } from '../gaps-training-start/gaps-training-start.service';
import { OptionsTrainingStartService } from '../options-training-start/options-training-start.service';
import { WordTranslateTrainingStartService } from '../word-translate-training-start/word-translate-training-start.service';
import { TrainingStartService } from './interface/training-start.service.interface';

@Injectable()
export class TrainingStartFactiory {
    constructor(private injector: Injector) {}

    create(mode: TrainingMode): TrainingStartService {
        switch (mode) {
            case TrainingMode.Cards:
                return this.injector.get(CardTrainingStartService);
            case TrainingMode.WordTranslate:
                return this.injector.get(WordTranslateTrainingStartService);
            case TrainingMode.Options:
                return this.injector.get(OptionsTrainingStartService);
            case TrainingMode.Gaps:
                return this.injector.get(GapsTrainingStartService);
            default:
                throw new Error(`Invalid parameter: ${mode}`);
        }
    }
}
