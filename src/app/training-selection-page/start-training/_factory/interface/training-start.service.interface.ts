export interface TrainingStartService {
    handle(): void;
}
