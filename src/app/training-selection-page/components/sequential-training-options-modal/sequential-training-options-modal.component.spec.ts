import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SequentialTrainingOptionsModalComponent } from './sequential-training-options-modal.component';

describe('SequentialTrainingOptionsModalComponent', () => {
    let component: SequentialTrainingOptionsModalComponent;
    let fixture: ComponentFixture<SequentialTrainingOptionsModalComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SequentialTrainingOptionsModalComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(
            SequentialTrainingOptionsModalComponent
        );
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
