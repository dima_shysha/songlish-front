export class DictionaryDataModel {
    countStudyWords: number;
    fromForeignToNative: boolean;

    constructor(init?: Partial<DictionaryDataModel>) {
        Object.assign(this, init);
    }
}
