import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SequentialTrainingOptionsModalComponent } from './sequential-training-options-modal.component';
import { DictionaryDataApiService } from './services/dictionary-data-api.service';
import { LoadingSpinnerModule } from 'src/app/_shared/loading-directive/loading-spinner/loading-spinner.module';
import { LanguagesSwapModule } from './components/languages-swap/languages-swap.module';

@NgModule({
    declarations: [SequentialTrainingOptionsModalComponent],
    providers: [DictionaryDataApiService],
    imports: [
        LoadingSpinnerModule,
        LanguagesSwapModule,
        CommonModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserAnimationsModule,
        MatButtonModule,
    ],
    exports: [SequentialTrainingOptionsModalComponent],
})
export class SequentialTrainingOptionsModalModule {}
