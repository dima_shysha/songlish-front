import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { take } from 'rxjs';
import { DEFAULT_TRAINING_SIZE } from 'src/app/trainings/_shared/sequential-trainings/constants/default-training-size.const';
import { SequentialTrainingOptionsModel } from 'src/app/_shared/trainings/models/sequential-training-options.model';
import { SequentialTrainingOptionsModalData } from './interface/sequential-training-options-modal-data';
import { DictionaryDataApiService } from './services/dictionary-data-api.service';

@Component({
    selector: 'sng-sequential-training-options-modal',
    templateUrl: './sequential-training-options-modal.component.html',
    styleUrls: ['./sequential-training-options-modal.component.scss'],
})
export class SequentialTrainingOptionsModalComponent implements OnInit {
    form: FormGroup;

    maxWordsCount: number;

    tooFewWords = false;

    isLoading = true;

    constructor(
        public dialogRef: MatDialogRef<SequentialTrainingOptionsModalComponent>,
        private dictionaryDataApiService: DictionaryDataApiService,
        private store: Store,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: SequentialTrainingOptionsModalData
    ) {}

    ngOnInit(): void {
        this.dictionaryDataApiService
            .getData()
            .pipe(take(1))
            .subscribe(x => {
                if (x.countStudyWords < 4) {
                    this.tooFewWords = true;
                    this.isLoading = false;
                    return;
                }

                this.maxWordsCount = x.countStudyWords;
                this.form = this.fb.group({
                    cardsCount: [
                        this.maxWordsCount > DEFAULT_TRAINING_SIZE
                            ? DEFAULT_TRAINING_SIZE
                            : this.maxWordsCount,
                        [Validators.required],
                    ],
                    reversed: [false, []],
                });
                this.isLoading = false;
            });
    }

    onSwapLanguages(reversed: boolean) {
        this.form.controls['reversed'].setValue(reversed);
    }

    onClose(): void {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
            return;
        }

        const formModel = this.form.getRawValue();
        const options = new SequentialTrainingOptionsModel({
            size: formModel.cardsCount,
            reversed: formModel.reversed,
        });
        this.dialogRef.close(options);
    }

    onGoToSongAdding(): void {
        this.store.dispatch(new Navigate(['song-search']));
        this.dialogRef.close();
    }
}
