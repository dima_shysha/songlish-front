import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DictionaryDataModel } from '../models/dictionary-data.model';

@Injectable()
export class DictionaryDataApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getData(): Observable<DictionaryDataModel> {
        return this.http.get<DictionaryDataModel>(`${this.url}/user/statistic`);
    }
}
