import { TestBed } from '@angular/core/testing';

import { DictionaryDataApiService } from './dictionary-data-api.service';

describe('DictionaryDataApiService', () => {
  let service: DictionaryDataApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DictionaryDataApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
