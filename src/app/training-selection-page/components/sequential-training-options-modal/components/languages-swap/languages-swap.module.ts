import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguagesSwapComponent } from './languages-swap.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [LanguagesSwapComponent],
    imports: [CommonModule, MatIconModule],
    exports: [LanguagesSwapComponent],
})
export class LanguagesSwapModule {}
