import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguagesSwapComponent } from './languages-swap.component';

describe('LanguagesSwapComponent', () => {
  let component: LanguagesSwapComponent;
  let fixture: ComponentFixture<LanguagesSwapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguagesSwapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguagesSwapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
