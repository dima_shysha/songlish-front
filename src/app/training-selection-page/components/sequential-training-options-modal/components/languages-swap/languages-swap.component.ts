import { Component, EventEmitter, HostListener, Output } from '@angular/core';

@Component({
    selector: 'sng-languages-swap',
    templateUrl: './languages-swap.component.html',
    styleUrls: ['./languages-swap.component.scss'],
})
export class LanguagesSwapComponent {
    @Output() swap = new EventEmitter<boolean>();

    @HostListener('click') onClick() {
        this.swapLanguages();
        this.isReversed = !this.isReversed;
        this.swap.emit(this.isReversed);
    }

    isReversed = false;

    sourceLanguage = 'английский';

    targetLanguage = 'русский';

    private swapLanguages(): void {
        [this.sourceLanguage, this.targetLanguage] = [
            this.targetLanguage,
            this.sourceLanguage,
        ];
    }
}
