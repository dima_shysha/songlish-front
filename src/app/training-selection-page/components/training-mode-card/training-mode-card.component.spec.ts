import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingModeCardComponent } from './training-mode-card.component';

describe('TrainingModeCardComponent', () => {
    let component: TrainingModeCardComponent;
    let fixture: ComponentFixture<TrainingModeCardComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TrainingModeCardComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TrainingModeCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
