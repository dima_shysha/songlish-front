import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingModeCardComponent } from './training-mode-card.component';

@NgModule({
    declarations: [TrainingModeCardComponent],
    imports: [CommonModule],
    exports: [TrainingModeCardComponent],
})
export class TrainingModeCardModule {}
