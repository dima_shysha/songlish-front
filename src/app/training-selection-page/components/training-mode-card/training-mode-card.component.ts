import { Component, Input } from '@angular/core';
import { TrainingModeDataModel } from './models/training-mode-data.model';

@Component({
    selector: 'sng-training-mode-card[mode]',
    templateUrl: './training-mode-card.component.html',
    styleUrls: ['./training-mode-card.component.scss'],
})
export class TrainingModeCardComponent {
    @Input() mode: TrainingModeDataModel;
}
