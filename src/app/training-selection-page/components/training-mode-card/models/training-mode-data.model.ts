export class TrainingModeDataModel {
    name: string;
    descriptionItems: string[];

    constructor(init?: Partial<TrainingModeDataModel>) {
        Object.assign(this, init);
    }
}
