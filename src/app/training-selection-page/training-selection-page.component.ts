import { Component } from '@angular/core';
import { TrainingMode } from './models/enums/training-mode.enum';
import { TrainingModeExtModel } from './models/training-mode-ext.model';
import { TrainingStartFactiory } from './start-training/_factory/training-start.factory';

@Component({
    selector: 'sng-training-selection-page',
    templateUrl: './training-selection-page.component.html',
    styleUrls: ['./training-selection-page.component.scss'],
})
export class TrainingSelectionPageComponent {
    trainingModeData: TrainingModeExtModel[] = [
        {
            name: 'Карточки',
            descriptionItems: [
                'Изменение уровня знания слов во время тренировки',
            ],
            mode: TrainingMode.Cards,
        },
        {
            name: 'Слово-перевод',
            descriptionItems: [
                'Изменение уровня знания слова во время тренировки',
                'Ввод одного из переводов слова',
            ],
            mode: TrainingMode.WordTranslate,
        },
        {
            name: 'Выбор из вариантов',
            descriptionItems: [
                'Изменение уровня знания слова во время тренировки',
                'Выбор перевода из вариантов',
            ],
            mode: TrainingMode.Options,
        },
        {
            name: 'Заполнение пропусков',
            descriptionItems: ['Изучение по контексту песни'],
            mode: TrainingMode.Gaps,
        },
    ];

    constructor(private trainingStartFactory: TrainingStartFactiory) {}

    onStartTraining(mode: TrainingMode) {
        this.trainingStartFactory.create(mode).handle();
    }
}
