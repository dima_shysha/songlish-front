import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingSelectionPageComponent } from './training-selection-page.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TrainingModeCardModule } from './components/training-mode-card/training-mode-card.module';
import { CardTrainingStartService } from './start-training/card-training-start/card-training-start.service';
import { OptionsTrainingStartService } from './start-training/options-training-start/options-training-start.service';
import { WordTranslateTrainingStartService } from './start-training/word-translate-training-start/word-translate-training-start.service';
import { TrainingStartFactiory } from './start-training/_factory/training-start.factory';
import { SequentialTrainingOptionsModalModule } from './components/sequential-training-options-modal/sequential-training-options-modal.module';
import { GapsTrainingStartService } from './start-training/gaps-training-start/gaps-training-start.service';
import { GapsTrainingStartOptionsModalModule } from './start-training/gaps-training-start/components/gaps-training-start-options-modal/gaps-training-start-options-modal.module';

@NgModule({
    declarations: [TrainingSelectionPageComponent],
    providers: [
        TrainingStartFactiory,
        CardTrainingStartService,
        WordTranslateTrainingStartService,
        OptionsTrainingStartService,
        GapsTrainingStartService,
    ],
    imports: [
        CommonModule,
        SequentialTrainingOptionsModalModule,
        GapsTrainingStartOptionsModalModule,
        TrainingModeCardModule,
        CommonModule,
        MatButtonModule,
        MatIconModule,
    ],
    exports: [TrainingSelectionPageComponent],
})
export class TrainingSelectionPageModule {}
