import { Route } from '@angular/router';
import { TrainingSelectionPageComponent } from '../training-selection-page.component';

export const TRAINING_SELECTION_PAGE: Route = {
    path: 'training-selection',
    component: TrainingSelectionPageComponent,
};
