import { TrainingModeDataModel } from '../components/training-mode-card/models/training-mode-data.model';
import { TrainingMode } from './enums/training-mode.enum';

export type TrainingModeExtModel = TrainingModeDataModel & {
    mode: TrainingMode;
};
