import { Route } from '@angular/router';
import { SongAddingPageResolver } from '../resolvers/song-adding-page.resolver';
import { SongAddingPageComponent } from '../song-adding-page.component';

export const SONG_ADDING_ROUTE: Route = {
    path: 'song-adding/:id',
    component: SongAddingPageComponent,
    resolve: { message: SongAddingPageResolver },
};
