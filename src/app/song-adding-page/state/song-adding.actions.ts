export class GetSong {
    static readonly type = '[SongAdding] Get song';

    constructor(public songId: string) {}
}

export class GetDistinctWords {
    static readonly type = '[SongAdding] Get distinct words';

    constructor(public songId: string) {}
}

export class AddWord {
    static readonly type = '[SongAdding] Add word';

    constructor(public wordId: string, public foreign: string) {}
}

export class RemoveWord {
    static readonly type = '[SongAdding] Remove word';

    constructor(public id: string) {}
}

export class RemoveTranslate {
    static readonly type = '[SongAdding] Remove translate';

    constructor(public wordId: string, public translate: string) {}
}

export class AddTranslate {
    static readonly type = '[SongAdding] Add translate';

    constructor(public wordId: string, public translate: string) {}
}

export class SaveAllWordsAndSong {
    static readonly type = '[SongAdding] Save all words and song';
}

export class SaveSelectedWordsAndSong {
    static readonly type = '[SongAdding] Save selected words and song';
}

export class ResetAddingSong {
    static readonly type = '[SongAdding] Reset adding song';
}
