export class WordInfoDto {
    translations: string[];
    status: string;
    word: {
        wordId: string;
        text: string;
        translations: string[];
    };

    constructor(init?: Partial<WordInfoDto>) {
        Object.assign(this, init);
    }
}
