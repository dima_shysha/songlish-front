import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, zip } from 'rxjs';
import { SongModel } from '../../models/song.model';
import { WordInfoModel } from '../../models/word-info.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { WordCardModel } from '../../components/word-card/models/word-card.interface';
import { WordInfoDto } from './dto/word-info.dto';

@Injectable()
export class SongAddingApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getSong(songId: string): Observable<SongModel> {
        return zip(
            this.http.get<SongModel>(`${this.url}/songs/${songId}`).pipe(
                map(song => {
                    song.text.forEach(item => {
                        item.isInDictionary = false;
                    });

                    return song;
                })
            ),
            this.getUserWordsIds()
        ).pipe(
            map(value => {
                const wordIds = value[1];
                const song = value[0];
                song.text.forEach(item => {
                    if (!item.wordId) {
                        return;
                    }

                    if (wordIds.includes(item.wordId)) {
                        item.isInDictionary = true;
                    }
                });

                return song;
            })
        );
    }

    getWordInfo(wordId: string): Observable<WordInfoModel> {
        return this.http
            .get<WordInfoDto>(`${this.url}/dictionary/${wordId}`)
            .pipe(
                map<WordInfoDto, WordInfoModel>(item => {
                    return {
                        translations: item.word.translations.map(x => x),
                    };
                }),
                catchError(() => {
                    return of(new WordInfoModel({ translations: [] }));
                })
            );
    }

    saveAllWords(songId: string): Observable<any> {
        return this.http.post(`${this.url}/dictionary/song/words`, { songId });
    }

    saveSelectedWords(words: WordCardModel[]): Observable<any> {
        return this.http.post(
            `${this.url}/dictionary`,
            words.map(x => {
                return {
                    wordId: x.id,
                    translations: x.translations,
                };
            })
        );
    }

    saveSong(songId: string): Observable<boolean> {
        return this.http.post<boolean>(`${this.url}/playlist/${songId}`, null);
    }

    private getUserWordsIds(): Observable<string[]> {
        return this.http
            .get<{ wordId: string }[]>(`${this.url}/dictionary/words`)
            .pipe(map(x => x.map(x => x.wordId)));
    }
}
