import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { catchError, Observable, of, tap, withLatestFrom } from 'rxjs';
import { SongModel } from '../models/song.model';
import { WordCardModel } from '../components/word-card/models/word-card.interface';
import { SongAddingApiService } from './services/song-adding-api.service';
import {
    GetDistinctWords,
    GetSong,
    AddWord,
    RemoveWord,
    RemoveTranslate,
    AddTranslate,
    SaveSelectedWordsAndSong,
    SaveAllWordsAndSong,
    ResetAddingSong,
} from './song-adding.actions';
import {
    OpenErrorSnack,
    OpenSuccessSnack,
} from 'src/app/_shared/snacks/state/snack.actions';
import {
    append,
    iif,
    insertItem,
    patch,
    removeItem,
    updateItem,
} from '@ngxs/store/operators';
import { WordInfoModel } from '../models/word-info.model';
import { LoadingService } from 'src/app/_shared/loading/services/loading.service';
import { AdditionItemModel } from '../models/addition-item.model';
import { Navigate } from '@ngxs/router-plugin';

export class SongAddingStateModel {
    song: SongModel;
    words: AdditionItemModel[];
    textItems: AdditionItemModel[];
    addedWords: WordCardModel[];
    isLoading: boolean;
}

const defaults: SongAddingStateModel = {
    song: null,
    words: [],
    textItems: [],
    addedWords: [],
    isLoading: false,
};

@State<SongAddingStateModel>({
    name: 'songAdding',
    defaults,
})
@Injectable()
export class SongAddingState {
    constructor(
        private wordsApiService: SongAddingApiService,
        private loadingService: LoadingService
    ) {}

    @Selector()
    static words(state: SongAddingStateModel): AdditionItemModel[] {
        return state.words;
    }

    @Selector()
    static textItems(state: SongAddingStateModel): AdditionItemModel[] {
        return state.textItems;
    }

    @Selector()
    static song(state: SongAddingStateModel): SongModel {
        return state.song;
    }

    @Selector()
    static addedWords(state: SongAddingStateModel): WordCardModel[] {
        return state.addedWords;
    }

    @Action(GetSong)
    onGetSong(
        { patchState }: StateContext<SongAddingStateModel>,
        { songId }: GetSong
    ): Observable<SongModel> {
        return this.wordsApiService.getSong(songId).pipe(
            tap(item => {
                patchState({
                    song: { ...item, id: songId },
                    textItems: item.text,
                });
            })
        );
    }

    @Action(GetDistinctWords)
    onGetDistinctWords({
        getState,
        setState,
    }: StateContext<SongAddingStateModel>): void {
        setState(
            patch({
                words: iif<AdditionItemModel[]>(
                    words => !words.length,
                    () => {
                        const { textItems } = getState();
                        return this.getDistinctWords(textItems);
                    }
                ),
            })
        );
    }

    @Action(AddWord)
    onAddWord(
        { setState }: StateContext<SongAddingStateModel>,
        { wordId, foreign }: AddWord
    ): Observable<WordInfoModel> {
        setState(
            patch({
                addedWords: insertItem({
                    id: wordId,
                    text: foreign,
                    isLoading: true,
                }),
            })
        );

        return this.wordsApiService.getWordInfo(wordId).pipe(
            tap(x => {
                setState(
                    patch({
                        addedWords: updateItem<WordCardModel>(
                            x => x.text === foreign,
                            patch<WordCardModel>({
                                translations: append(x.translations),
                                isLoading: false,
                            })
                        ),
                    })
                );
            })
        );
    }

    @Action(RemoveWord)
    onRemoveWord(
        { setState }: StateContext<SongAddingStateModel>,
        { id }: RemoveWord
    ): void {
        setState(
            patch({
                addedWords: removeItem<WordCardModel>(x => x.id === id),
            })
        );
    }

    @Action(RemoveTranslate)
    onRemoveTranslate(
        { setState }: StateContext<SongAddingStateModel>,
        { wordId, translate }: RemoveTranslate
    ): void {
        setState(
            patch({
                addedWords: updateItem<WordCardModel>(
                    x => x.id === wordId,
                    patch({
                        translations: removeItem<string>(x => x === translate),
                    })
                ),
            })
        );
    }

    @Action(AddTranslate)
    onAddTranslate(
        { setState }: StateContext<SongAddingStateModel>,
        { wordId, translate }: AddTranslate
    ): void {
        setState(
            patch({
                addedWords: updateItem<WordCardModel>(
                    x => x.id === wordId,
                    patch({
                        translations: append([translate]),
                    })
                ),
            })
        );
    }

    @Action(SaveAllWordsAndSong)
    onSaveAllWords({
        getState,
        dispatch,
    }: StateContext<SongAddingStateModel>): Observable<any> {
        this.loadingService.open('Добавляем слова в словарь');

        const { song } = getState();
        return this.wordsApiService.saveAllWords(song.id).pipe(
            withLatestFrom(this.wordsApiService.saveSong(song.id)),
            tap(() => {
                dispatch([
                    new Navigate(['duh']),
                    new OpenSuccessSnack('Слова добавлены в словарь'),
                ]);
                this.loadingService.close();
            }),
            catchError(_ => {
                dispatch([
                    new Navigate(['duh']),
                    new OpenErrorSnack('Произошла ошибка'),
                ]);
                this.loadingService.close();
                return of(null);
            })
        );
    }

    @Action(SaveSelectedWordsAndSong)
    onSaveSelectedWordsAndSong({
        getState,
        dispatch,
    }: StateContext<SongAddingStateModel>): Observable<any> {
        this.loadingService.open('Добавляем выбранные слова в словарь');

        const { addedWords, song } = getState();
        return this.wordsApiService.saveSelectedWords(addedWords).pipe(
            withLatestFrom(this.wordsApiService.saveSong(song.id)),
            tap(() => {
                this.loadingService.close();
                dispatch([
                    new Navigate(['duh']),
                    new OpenSuccessSnack('Слова добавлены в словарь'),
                ]);
            }),
            catchError(_ => {
                dispatch([
                    new Navigate(['duh']),
                    new OpenErrorSnack('Произошла ошибка'),
                ]);
                this.loadingService.close();
                return of(null);
            })
        );
    }

    @Action(ResetAddingSong)
    onResetAddingSong({ setState }: StateContext<SongAddingStateModel>): void {
        setState(defaults);
    }

    private getDistinctWords(items: AdditionItemModel[]): AdditionItemModel[] {
        let processedWords = items.filter(x => x.wordId);
        const key = 'wordId';
        processedWords = [
            ...new Map(processedWords.map(item => [item[key], item])).values(),
        ];
        processedWords.forEach(x => (x.text = x.text.toLowerCase()));
        processedWords = processedWords.sort((x1, x2) =>
            x1.text.localeCompare(x2.text)
        );
        return processedWords;
    }
}
