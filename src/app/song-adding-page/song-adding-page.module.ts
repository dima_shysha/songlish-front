import { NgModule } from '@angular/core';
import { SongAddingPageComponent } from './song-adding-page.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SongAddingState } from './state/song-adding.state';
import { NgxsModule } from '@ngxs/store';
import { WordCardModule } from './components/word-card/word-card.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SongAddingApiService } from './state/services/song-adding-api.service';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { TextItemModule } from './components/text-item/text-item.module';
import { ListItemModule } from './components/list-item/list-item.module';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { YoutubePlayerModule } from '../_shared/youtube-player/youtube-player.module';
import { ExitBtnModule } from './components/exit-btn/exit-btn.module';
import { LoadingDirectiveModule } from '../_shared/loading-directive/loading-directive.module';

@NgModule({
    declarations: [SongAddingPageComponent],
    providers: [SongAddingApiService],
    imports: [
        WordCardModule,
        TextItemModule,
        ListItemModule,
        YoutubePlayerModule,
        LoadingDirectiveModule,
        ExitBtnModule,
        NgxsModule.forFeature([SongAddingState]),
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule,
        MatTabsModule,
        MatMenuModule,
        HttpClientModule,
        MatButtonToggleModule,
        MatBadgeModule,
    ],
    exports: [SongAddingPageComponent],
})
export class SongAddingPageModule {}
