import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SongAddingPageComponent } from './song-adding-page.component';

describe('SongAddingPageComponent', () => {
    let component: SongAddingPageComponent;
    let fixture: ComponentFixture<SongAddingPageComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SongAddingPageComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(SongAddingPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
