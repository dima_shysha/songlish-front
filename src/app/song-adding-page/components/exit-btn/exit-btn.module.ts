import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExitBtnComponent } from './exit-btn.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [ExitBtnComponent],
    imports: [CommonModule, MatButtonModule],
    exports: [ExitBtnComponent],
})
export class ExitBtnModule {}
