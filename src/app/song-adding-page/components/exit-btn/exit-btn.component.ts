import { Component } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';

@Component({
    selector: 'sng-exit-btn',
    templateUrl: './exit-btn.component.html',
    styleUrls: ['./exit-btn.component.scss'],
})
export class ExitBtnComponent {
    constructor(private store: Store) {}

    onClick() {
        this.store.dispatch(new Navigate(['song-search']));
    }
}
