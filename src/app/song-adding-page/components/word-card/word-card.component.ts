import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { WordCardModel } from './models/word-card.interface';

@Component({
    selector: 'sng-word-card',
    templateUrl: './word-card.component.html',
    styleUrls: ['./word-card.component.scss'],
})
export class WordCardComponent {
    @Input() word: WordCardModel;

    @Output() remove = new EventEmitter();

    @Output() translateAdd = new EventEmitter<string>();

    @Output() translateRemove = new EventEmitter<string>();

    readonly separatorKeysCodes = [ENTER, COMMA] as const;

    readonly maxTranslatesAmount = 5 as const;

    onWordRemove(): void {
        this.remove.emit();
    }

    onTranslateRemove(translate: string): void {
        this.translateRemove.emit(translate);
    }

    onTranslateAdd($event: MatChipInputEvent): void {
        if (!($event.value || '').trim()) {
            $event.chipInput.clear();
            return;
        }

        this.translateAdd.emit($event.value);
    }
}
