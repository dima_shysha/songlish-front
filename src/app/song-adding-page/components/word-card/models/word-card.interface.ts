import { IsLoading } from 'src/app/_shared/interfaces/is-loading.interface';

export interface WordCardModel extends IsLoading {
    id: string;
    text: string;
    translations: string[];
}
