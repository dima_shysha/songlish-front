import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordCardComponent } from './word-card.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
    declarations: [WordCardComponent],
    imports: [
        CommonModule,
        MatChipsModule,
        MatIconModule,
        MatFormFieldModule,
        MatButtonModule,
        MatProgressSpinnerModule,
    ],
    exports: [WordCardComponent],
})
export class WordCardModule {}
