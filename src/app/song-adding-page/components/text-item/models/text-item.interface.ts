export interface TextItemModel {
    wordId?: string;
    text: string;
    isInDictionary: boolean;
}
