import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';
import { TextItemModel } from './models/text-item.interface';

@Component({
    selector: 'sng-text-item[word]',
    templateUrl: './text-item.component.html',
    styleUrls: ['./text-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextItemComponent {
    @Input() word: TextItemModel;

    @Input() isSelected = false;

    @Output() changeSelection = new EventEmitter();

    onChangeSelection(): void {
        this.changeSelection.emit();
    }
}
