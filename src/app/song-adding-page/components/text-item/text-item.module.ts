import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextItemComponent } from './text-item.component';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
    declarations: [TextItemComponent],
    imports: [CommonModule, MatTooltipModule],
    exports: [TextItemComponent],
})
export class TextItemModule {}
