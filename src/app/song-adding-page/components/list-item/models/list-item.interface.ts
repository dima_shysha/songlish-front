export interface ListItemModel {
    wordId?: string;
    text: string;
    isInDictionary: boolean;
}
