import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ListItemModel } from './models/list-item.interface';

@Component({
    selector: 'sng-list-item[word]',
    templateUrl: './list-item.component.html',
    styleUrls: ['./list-item.component.scss'],
})
export class ListItemComponent {
    @Input() word: ListItemModel;

    @Input() isSelected = false;

    @Output() changeSelection = new EventEmitter();

    onChangeSelection(): void {
        this.changeSelection.emit();
    }
}
