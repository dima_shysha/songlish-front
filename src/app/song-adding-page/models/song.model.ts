import { AdditionItemModel } from './addition-item.model';

export class SongModel {
    id: string;
    authorName: string;
    songName: string;
    videoUri: string;
    wordCount: number;
    uniqueWordCount: number;
    text: AdditionItemModel[];

    constructor(init?: Partial<SongModel>) {
        Object.assign(this, init);
    }
}
