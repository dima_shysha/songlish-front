export class WordToSaveModel {
    id: string;
    foreign: string;
    translates: string[];

    // extention
    isLoading = false;

    constructor(init?: Partial<WordToSaveModel>) {
        Object.assign(this, init);
    }
}
