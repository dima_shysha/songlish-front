export class WordInfoModel {
    translations: string[];

    constructor(init?: Partial<WordInfoModel>) {
        Object.assign(this, init);
    }
}
