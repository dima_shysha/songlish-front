import { ListItemModel } from '../components/list-item/models/list-item.interface';
import { TextItemModel } from '../components/text-item/models/text-item.interface';

export class AdditionItemModel implements ListItemModel, TextItemModel {
    wordId?: string;
    text: string;
    isInDictionary: boolean;

    constructor(init?: Partial<AdditionItemModel>) {
        Object.assign(this, init);
    }
}
