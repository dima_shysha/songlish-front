import { WordCardModel } from '../components/word-card/models/word-card.interface';

export class WordCardImplModel implements WordCardModel {
    id: string;
    text: string;
    translations: string[];
    isLoading: boolean;

    constructor(init?: Partial<WordCardImplModel>) {
        Object.assign(this, init);
    }
}
