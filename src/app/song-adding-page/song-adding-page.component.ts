import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { filter, Observable } from 'rxjs';
import { SongModel } from './models/song.model';
import {
    AddTranslate,
    AddWord,
    GetDistinctWords,
    RemoveTranslate,
    RemoveWord,
    ResetAddingSong,
    SaveAllWordsAndSong,
    SaveSelectedWordsAndSong,
} from './state/song-adding.actions';
import { SongAddingState } from './state/song-adding.state';
import {
    MatTooltipDefaultOptions,
    MAT_TOOLTIP_DEFAULT_OPTIONS,
} from '@angular/material/tooltip';
import { SubSink } from 'subsink';
import { ScreenHelper } from '../_shared/helpers/screen.helper';
import { WordCardModel } from './components/word-card/models/word-card.interface';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { AdditionItemModel } from './models/addition-item.model';
import { Navigate } from '@ngxs/router-plugin';

export const myCustomTooltipDefaults: MatTooltipDefaultOptions = {
    showDelay: 1500,
    hideDelay: 0,
    touchendHideDelay: 0,
};

@Component({
    selector: 'sng-song-adding-page',
    templateUrl: './song-adding-page.component.html',
    styleUrls: ['./song-adding-page.component.scss'],
    providers: [
        {
            provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
            useValue: myCustomTooltipDefaults,
        },
    ],
})
export class SongAddingPageComponent implements OnInit, OnDestroy {
    @Select(SongAddingState.song) song$: Observable<SongModel>;

    @Select(SongAddingState.words) distinctWords$: Observable<
        AdditionItemModel[]
    >;

    @Select(SongAddingState.textItems) textItems$: Observable<
        AdditionItemModel[]
    >;

    @Select(SongAddingState.addedWords) addedWords$: Observable<
        WordCardModel[]
    >;

    isMobile$ = ScreenHelper.isMobile$;

    isTablet$ = ScreenHelper.isTablet$;

    songId: string;

    addedWords: WordCardModel[];

    currentTextView: 'text' | 'list' = 'text';

    isLoading = true;

    protected sub = new SubSink();

    constructor(private store: Store, private cdr: ChangeDetectorRef) {}

    ngOnInit(): void {
        this.sub.add(
            this.addedWords$.subscribe(items => {
                this.addedWords = items;
            }),
            this.song$.pipe(filter(x => !!x)).subscribe(item => {
                this.songId = item?.id;
                this.isLoading = false;
            })
        );
    }

    ngOnDestroy(): void {
        this.store.dispatch(new ResetAddingSong());
        this.sub.unsubscribe();
    }

    onWordAdd(word: AdditionItemModel): void {
        this.store.dispatch(new AddWord(word.wordId, word.text));
    }

    onWordRemove(id: string): void {
        this.store.dispatch(new RemoveWord(id));
    }

    onTranslateRemove(wordId: string, translate: string): void {
        this.store.dispatch(new RemoveTranslate(wordId, translate));
    }

    onTranslateAdd(wordId: string, foreign: string): void {
        this.store.dispatch(new AddTranslate(wordId, foreign));
    }

    onSaveSelectedWords(): void {
        this.store.dispatch(new SaveSelectedWordsAndSong());
    }

    onSaveAllWords(): void {
        this.store.dispatch(new SaveAllWordsAndSong());
    }

    isSelected(word: AdditionItemModel): boolean {
        return this.addedWords.some(x => x.text === word.text);
    }

    onTextViewModeChange(event: MatButtonToggleChange): void {
        const value = event.value;
        this.currentTextView = value;
        if (this.currentTextView === 'list') {
            this.store.dispatch(new GetDistinctWords(this.songId));
        }
    }

    onLeave(): void {
        this.store.dispatch(new Navigate(['duh']));
    }
}
