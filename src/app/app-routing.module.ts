import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationPageComponent } from './auth/components/registration-page/registration-page.component';
import { LoginPageComponent } from './auth/components/login-page/login-page.component';
import { SONG_ADDING_ROUTE } from './song-adding-page/routing/song-adding.route';
import { CARD_TRAINING_ROUTE } from './trainings/card-training-page/routing/card-training.route';
import { FILL_GAPS_TRAINING_ROUTE } from './trainings/gaps-training/routing/gaps-training.route';
import { WORD_TRANSLATE_TRAINING_PAGE } from './trainings/word-translate-training-page/routing/word-translate-training-page.route';
import { OPTIONS_TRAINING_ROUTE } from './trainings/options-training/routing/options-training.route';
import { SONG_SEARCH_ROUTE } from './song-search-page/routing/song-search.route';
import { TRAINING_SELECTION_PAGE } from './training-selection-page/routing/training-selection-page.route';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            SONG_SEARCH_ROUTE,
            TRAINING_SELECTION_PAGE,
            {
                path: 'songs',
                loadChildren: () =>
                    import('./songs-page/songs-page.module').then(
                        m => m.SongsPageModule
                    ),
            },
            {
                path: 'words-table',
                loadChildren: () =>
                    import('./words-table/words-table.module').then(
                        m => m.WordsTableModule
                    ),
            },
            {
                path: '',
                redirectTo: SONG_SEARCH_ROUTE.path,
                pathMatch: 'full',
            },
        ],
    },
    SONG_ADDING_ROUTE,
    {
        path: 'training',
        children: [
            CARD_TRAINING_ROUTE,
            FILL_GAPS_TRAINING_ROUTE,
            WORD_TRANSLATE_TRAINING_PAGE,
            OPTIONS_TRAINING_ROUTE,
        ],
    },
    {
        path: 'registration',
        component: RegistrationPageComponent,
    },
    {
        path: 'login',
        component: LoginPageComponent,
    },
    {
        path: '',
        redirectTo: SONG_SEARCH_ROUTE.path,
        pathMatch: 'full',
    },
    {
        path: '**',
        redirectTo: '',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
