import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { AuthApiService } from '../auth/services/auth-api.service';
import { SidenavService } from '../_shared/sidenav/sidenav.service';

@Component({
    selector: 'sng-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
    @ViewChild('sidenav') sidenav: MatSidenav;

    constructor(
        private authService: AuthApiService,
        private store: Store,
        private sidenavService: SidenavService
    ) {}

    ngAfterViewInit(): void {
        this.sidenavService.setSidenav(this.sidenav);
    }

    onLogout(): void {
        this.authService.logout();
        this.store.dispatch(new Navigate(['login']));
    }
}
