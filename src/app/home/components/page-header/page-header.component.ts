import { Component, EventEmitter, Output } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { AuthApiService } from 'src/app/auth/services/auth-api.service';
import { ScreenHelper } from 'src/app/_shared/helpers/screen.helper';
import { SidenavService } from 'src/app/_shared/sidenav/sidenav.service';

@Component({
    selector: 'sng-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent {
    @Output() toggleSidenav = new EventEmitter();

    isTablet$ = ScreenHelper.isTablet$;

    constructor(
        private store: Store,
        private authService: AuthApiService,
        private sidenavService: SidenavService
    ) {}

    onToggleSidenav(): void {
        this.sidenavService.toggle();
    }

    onLogout(): void {
        this.authService.logout();
        this.store.dispatch(new Navigate(['login']));
    }
}
