import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { PageHeaderModule } from './components/page-header/page-header.module';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        PageHeaderModule,
        RouterModule,

        //
        BrowserModule,
        MatSidenavModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
    ],
    exports: [HomeComponent],
})
export class HomeModule {}
