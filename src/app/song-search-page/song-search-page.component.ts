import { Component } from '@angular/core';

@Component({
    selector: 'sng-song-search-page',
    templateUrl: './song-search-page.component.html',
    styleUrls: ['./song-search-page.component.scss'],
})
export class SongSearchPageComponent {}
