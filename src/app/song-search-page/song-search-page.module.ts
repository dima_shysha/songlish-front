import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongSearchApiService } from './services/song-search-api.service';
import { LargeSearchFieldModule } from './song-search-field/song-search-field.module';
import { SongSearchPageComponent } from './song-search-page.component';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { PopularSongsTableModule } from './popular-songs-table/popular-songs-table.module';

@NgModule({
    declarations: [SongSearchPageComponent],
    providers: [SongSearchApiService],
    imports: [
        CommonModule,
        LargeSearchFieldModule,
        PopularSongsTableModule,
        MatButtonModule,
        RouterModule,
    ],
    exports: [SongSearchPageComponent],
})
export class SongSearchPageModule {}
