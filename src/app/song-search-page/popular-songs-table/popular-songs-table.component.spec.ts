import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularSongsTableComponent } from './popular-songs-table.component';

describe('SongsTableComponent', () => {
    let component: PopularSongsTableComponent;
    let fixture: ComponentFixture<PopularSongsTableComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PopularSongsTableComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PopularSongsTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
