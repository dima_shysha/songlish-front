import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopularSongsTableComponent } from './popular-songs-table.component';
import { NgxsModule } from '@ngxs/store';
import { PopularSongsState } from './songs-state/popular-songs.state';
import { PopularSongsApiService } from './songs-state/services/popular-songs-api.service';
import { PopularSongListTileModule } from './components/popular-song-list-tile/popular-song-list-tile.module';
import { SearchFieldModule } from 'src/app/words-table/components/search-field/search-field.module';
import { LoadingDirectiveModule } from 'src/app/_shared/loading-directive/loading-directive.module';

@NgModule({
    declarations: [PopularSongsTableComponent],
    providers: [PopularSongsApiService],
    imports: [
        NgxsModule.forFeature([PopularSongsState]),
        LoadingDirectiveModule,
        PopularSongListTileModule,
        SearchFieldModule,
        CommonModule,
    ],
    exports: [PopularSongsTableComponent],
})
export class PopularSongsTableModule {}
