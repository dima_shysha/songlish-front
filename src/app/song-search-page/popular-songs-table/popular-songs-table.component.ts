import { Component, OnInit } from '@angular/core';
import { PopularSongListItemModel } from './models/popular-song-list-item.model';
import { Select, Store } from '@ngxs/store';
import { GetPopularSongs } from './songs-state/popular-songs.actions';
import { Navigate } from '@ngxs/router-plugin';
import { Observable } from 'rxjs';
import { PopularSongsState } from './songs-state/popular-songs.state';

@Component({
    selector: 'sng-popular-songs-table',
    templateUrl: './popular-songs-table.component.html',
    styleUrls: ['./popular-songs-table.component.scss'],
})
export class PopularSongsTableComponent implements OnInit {
    @Select(PopularSongsState.items) songs$: Observable<
        PopularSongListItemModel[]
    >;

    isLoading = true;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.store.dispatch(new GetPopularSongs()).subscribe(() => {
            this.isLoading = false;
        });
    }

    onAddSong(songId: string): void {
        this.store.dispatch(new Navigate([`song-adding/${songId}`]));
    }
}
