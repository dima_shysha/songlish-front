import { PopularSongListItem } from '../components/popular-song-list-tile/interfaces/popular-song-list-item.interface';

export class PopularSongListItemModel implements PopularSongListItem {
    id: string;
    name: string;
    authorName: string;

    constructor(init?: Partial<PopularSongListItemModel>) {
        Object.assign(this, init);
    }
}
