import { TestBed } from '@angular/core/testing';

import { PopularSongsApiService } from './popular-songs-api.service';

describe('PopularSongsApiService', () => {
    let service: PopularSongsApiService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(PopularSongsApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
