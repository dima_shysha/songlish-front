import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PopularSongListItemModel } from '../../models/popular-song-list-item.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PopularSongsApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getAll(): Observable<PopularSongListItemModel[]> {
        return this.http.get<PopularSongListItemModel[]>(
            `${this.url}/songs/popular?size=${20}`
        );
    }
}
