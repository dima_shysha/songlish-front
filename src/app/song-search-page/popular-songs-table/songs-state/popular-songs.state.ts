import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Observable, tap } from 'rxjs';
import { PopularSongListItemModel } from '../models/popular-song-list-item.model';
import { PopularSongsApiService } from './services/popular-songs-api.service';
import { GetPopularSongs } from './popular-songs.actions';

export class PopularSongsStateModel {
    public items: PopularSongListItemModel[];
}

const defaults = {
    items: [],
} as PopularSongsStateModel;

@State<PopularSongsStateModel>({
    name: 'popularSongs',
    defaults,
})
@Injectable()
export class PopularSongsState {
    constructor(private songsApiService: PopularSongsApiService) {}

    @Selector()
    static items(state: PopularSongsStateModel): PopularSongListItemModel[] {
        return state.items;
    }

    @Action(GetPopularSongs)
    onGetPopularSongs({
        patchState,
    }: StateContext<PopularSongsStateModel>): Observable<
        PopularSongListItemModel[]
    > {
        return this.songsApiService.getAll().pipe(
            tap(items => {
                patchState({ items });
            })
        );
    }
}
