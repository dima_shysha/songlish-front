import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';
import { PopularSongListItem } from './interfaces/popular-song-list-item.interface';

@Component({
    selector: 'sng-popular-song-list-tile[song]',
    templateUrl: './popular-song-list-tile.component.html',
    styleUrls: ['./popular-song-list-tile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PopularSongListTileComponent {
    @Input() song: PopularSongListItem;

    @Output() addSong = new EventEmitter();

    onAddSong(): void {
        this.addSong.emit();
    }
}
