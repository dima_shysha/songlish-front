export interface PopularSongListItem {
    id: string;
    name: string;
    authorName: string;
}
