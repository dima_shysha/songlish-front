import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularSongListTileComponent } from './popular-song-list-tile.component';

describe('PopularSongListTileComponent', () => {
    let component: PopularSongListTileComponent;
    let fixture: ComponentFixture<PopularSongListTileComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PopularSongListTileComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PopularSongListTileComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
