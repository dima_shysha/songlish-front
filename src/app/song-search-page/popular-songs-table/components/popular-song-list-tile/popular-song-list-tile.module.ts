import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopularSongListTileComponent } from './popular-song-list-tile.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [PopularSongListTileComponent],
    imports: [CommonModule, MatButtonModule],
    exports: [PopularSongListTileComponent],
})
export class PopularSongListTileModule {}
