import { TestBed } from '@angular/core/testing';

import { SongSearchApiService } from './song-search-api.service';

describe('SongSearchApiService', () => {
  let service: SongSearchApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SongSearchApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
