export class SongSearchResultModel {
    songId: string;
    authorName: string;
    name: string;

    constructor(init?: Partial<SongSearchResultModel>) {
        Object.assign(this, init);
    }
}
