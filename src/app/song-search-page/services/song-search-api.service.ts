import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { SongSearchResultModel } from 'src/app/song-search-page/services/models/song-search-result.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class SongSearchApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    search(query: string): Observable<SongSearchResultModel[]> {
        return this.http
            .get<SongSearchResultModel[]>(`${this.url}/search/song/${query}`)
            .pipe(
                catchError(_ => {
                    return of(null);
                })
            );
    }
}
