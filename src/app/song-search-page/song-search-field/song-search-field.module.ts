import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LargeSearchFieldComponent } from './song-search-field.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SongSearchApiService } from '../services/song-search-api.service';
import { HighlightPipeModule } from 'src/app/_shared/highlight-pipe/highlight-pipe.module';

@NgModule({
    declarations: [LargeSearchFieldComponent],
    providers: [SongSearchApiService],
    imports: [
        HighlightPipeModule,
        CommonModule,
        MatAutocompleteModule,
        MatIconModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
    ],
    exports: [LargeSearchFieldComponent],
})
export class LargeSearchFieldModule {}
