import {
    Component,
    Output,
    EventEmitter,
    ViewChild,
    OnInit,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import {
    MatAutocomplete,
    MatAutocompleteActivatedEvent,
    MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import {
    Observable,
    debounceTime,
    distinctUntilChanged,
    mergeMap,
    tap,
    of,
} from 'rxjs';
import { SongSearchApiService } from '../services/song-search-api.service';

@Component({
    selector: 'sng-song-search-field',
    templateUrl: './song-search-field.component.html',
    styleUrls: ['./song-search-field.component.scss'],
})
export class LargeSearchFieldComponent implements OnInit {
    @Output() search = new EventEmitter<string>();

    @ViewChild('auto') autocomplete: MatAutocomplete;

    myControl = new FormControl();

    filteredOptions$?: Observable<string[]>;

    toHighlight: string = '';

    isLoading = false;

    constructor(
        private searchApiService: SongSearchApiService,
        private store: Store
    ) {}

    ngOnInit() {
        this.filteredOptions$ = this.myControl.valueChanges.pipe(
            tap(x => {
                this.isLoading = true;
                this.toHighlight = x;
            }),
            debounceTime(300),
            distinctUntilChanged(),
            mergeMap(x => {
                if (!x) {
                    return of([]);
                }

                return this.searchApiService.search(x);
            }),
            tap(() => {
                this.isLoading = false;
            })
        );
    }

    onOptionActivated($event: MatAutocompleteActivatedEvent): void {
        const value = $event.option?.value;
        this.myControl.setValue(`${value.authorName} ${value.name}`, {
            emitEvent: false,
        });
    }

    onOptionSelected($event: MatAutocompleteSelectedEvent): void {
        const value = $event.option?.value;
        this.store.dispatch(new Navigate(['song-adding', value.songId]));
    }
}
