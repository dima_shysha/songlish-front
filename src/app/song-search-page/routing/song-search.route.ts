import { Route } from '@angular/router';
import { SongSearchPageComponent } from '../song-search-page.component';

export const SONG_SEARCH_ROUTE: Route = {
    path: 'song-search',
    component: SongSearchPageComponent,
};
