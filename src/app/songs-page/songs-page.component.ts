import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'sng-songs-page',
    templateUrl: './songs-page.component.html',
    styleUrls: ['./songs-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SongsPageComponent {}
