import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongsPageComponent } from './songs-page.component';
import { SongsTableModule } from './songs-table/songs-table.module';
import { SongsPageRoutingModule } from './songs-page-routing.module';

@NgModule({
    declarations: [SongsPageComponent],
    imports: [SongsPageRoutingModule, SongsTableModule, CommonModule],
    exports: [SongsPageComponent],
})
export class SongsPageModule {}
