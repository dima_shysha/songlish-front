import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnInit,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SongDetailsModalComponent } from './components/song-details-modal/song-details-modal.component';
import { SongListItemModel } from './models/song-list-item.model';
import { GetSongs, SetSongsSearchQuery } from './songs-state/songs.actions';
import { SongsQuery } from './songs-state/songs.query';

@Component({
    selector: 'sng-songs-table',
    templateUrl: './songs-table.component.html',
    styleUrls: ['./songs-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SongsTableComponent implements OnInit {
    @Select(SongsQuery.songs) songs$: Observable<SongListItemModel[]>;

    isLoading = true;

    noSongsAdded = false;

    constructor(
        private store: Store,
        private dialog: MatDialog,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.store.dispatch(new GetSongs()).subscribe(x => {
            this.noSongsAdded = !x.songs.items.length;
            this.isLoading = false;
            this.cdr.markForCheck();
        });
    }

    onOpenDetails(songId: string): void {
        this.dialog.open(SongDetailsModalComponent, {
            data: {
                songId,
            },
        });
    }

    onSearchInput(query: string): void {
        this.store.dispatch(new SetSongsSearchQuery(query));
    }

    onGoToSongAdding(): void {
        this.store.dispatch(new Navigate(['song-search']));
    }
}
