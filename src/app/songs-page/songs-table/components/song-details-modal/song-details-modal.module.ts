import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongDetailsModalComponent } from './song-details-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { SongDetailsApiService } from './state/services/song-details-api.service';
import { SubSink } from 'subsink';
import { NgxsModule } from '@ngxs/store';
import { SongDetailsState } from './state/song-details.state';
import { LoadingSpinnerModule } from 'src/app/_shared/loading-directive/loading-spinner/loading-spinner.module';
import { YoutubePlayerModule } from 'src/app/_shared/youtube-player/youtube-player.module';

@NgModule({
    declarations: [SongDetailsModalComponent],
    providers: [SongDetailsApiService, SubSink],
    imports: [
        NgxsModule.forFeature([SongDetailsState]),
        YoutubePlayerModule,
        LoadingSpinnerModule,
        CommonModule,
        MatDialogModule,
        MatIconModule,
    ],
    exports: [SongDetailsModalComponent],
})
export class SongDetailsModalModule {}
