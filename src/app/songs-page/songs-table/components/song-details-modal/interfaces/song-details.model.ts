export class SongDetailsModel {
    videoUri: string;
    text: string;
    authorName: string;
    songName: string;

    constructor(init?: Partial<SongDetailsModel>) {
        Object.assign(this, init);
    }
}
