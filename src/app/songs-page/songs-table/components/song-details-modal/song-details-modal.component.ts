import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { filter, Observable } from 'rxjs';
import { SubSink } from 'subsink';
import { SongDetailsModel } from './interfaces/song-details.model';
import { GetSongDetails } from './state/song-details.actions';
import { SongDetailsState } from './state/song-details.state';

@Component({
    selector: 'sng-song-details-modal',
    templateUrl: './song-details-modal.component.html',
    styleUrls: ['./song-details-modal.component.scss'],
})
export class SongDetailsModalComponent implements OnInit, OnDestroy {
    @Select(SongDetailsState.songDetails)
    songDetails$: Observable<SongDetailsModel>;

    songDetails: SongDetailsModel;

    songId: string;

    isLoading = true;

    constructor(
        public dialogRef: MatDialogRef<SongDetailsModalComponent>,
        private store: Store,
        private sub: SubSink,
        @Inject(MAT_DIALOG_DATA) public data: { songId: string }
    ) {
        this.songId = data.songId;
    }

    ngOnInit(): void {
        this.store.dispatch(new GetSongDetails(this.songId)).subscribe(() => {
            this.isLoading = false;
        });

        this.sub.add(
            this.songDetails$.pipe(filter(x => !!x)).subscribe(x => {
                this.songDetails = x;
            })
        );
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
}
