export class GetSongDetails {
    static readonly type = '[SongDetails] Get song details';

    constructor(public songId: string) {}
}
