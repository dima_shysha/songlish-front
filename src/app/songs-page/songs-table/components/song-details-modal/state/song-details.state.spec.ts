import { TestBed, async } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { SongDetailsState } from './song-details.state';
import { GetSongDetails } from './song-details.actions';

describe('SongDetails actions', () => {
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([SongDetailsState])],
        }).compileComponents();
        store = TestBed.get(Store);
    }));

    it('should create an action and add an item', () => {
        store.dispatch(new GetSongDetails('item-1'));
        store
            .select(state => state.songDetails.items)
            .subscribe((items: string[]) => {
                expect(items).toEqual(jasmine.objectContaining(['item-1']));
            });
    });
});
