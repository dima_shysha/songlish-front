import { TestBed } from '@angular/core/testing';

import { SongDetailsApiService } from './song-details-api.service';

describe('SongDetailsApiService', () => {
  let service: SongDetailsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SongDetailsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
