import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SongDetailsModel } from '../../interfaces/song-details.model';

@Injectable()
export class SongDetailsApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getSong(songId: string): Observable<SongDetailsModel> {
        return this.http.get<any>(`${this.url}/songs/${songId}`).pipe(
            map(song => {
                const text = song.text.map(x => x.text).join('');
                return new SongDetailsModel({
                    authorName: song.authorName,
                    songName: song.songName,
                    videoUri: song.videoUri,
                    text,
                });
            })
        );
    }
}
