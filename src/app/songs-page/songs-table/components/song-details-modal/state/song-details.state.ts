import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Observable, tap } from 'rxjs';
import { SongDetailsModel } from '../interfaces/song-details.model';
import { SongDetailsApiService } from './services/song-details-api.service';
import { GetSongDetails } from './song-details.actions';

export class SongDetailsStateModel {
    public songDetails: SongDetailsModel;
}

const defaults = {
    songDetails: null,
} as SongDetailsStateModel;

@State<SongDetailsStateModel>({
    name: 'songDetails',
    defaults,
})
@Injectable()
export class SongDetailsState {
    @Selector()
    static songDetails(state: SongDetailsStateModel): SongDetailsModel {
        return state.songDetails;
    }

    constructor(private songDetailsApiService: SongDetailsApiService) {}

    @Action(GetSongDetails)
    onGetSongDetails(
        { patchState }: StateContext<SongDetailsStateModel>,
        { songId }: GetSongDetails
    ): Observable<SongDetailsModel> {
        return this.songDetailsApiService.getSong(songId).pipe(
            tap(songDetails => {
                patchState({ songDetails });
            })
        );
    }
}
