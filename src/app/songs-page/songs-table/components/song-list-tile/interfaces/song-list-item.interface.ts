export interface SongListItem {
    songId: string;
    playlistId: string;
    authorName: string;
    songName: string;
    status: string;
}
