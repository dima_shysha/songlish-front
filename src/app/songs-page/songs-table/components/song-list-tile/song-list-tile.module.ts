import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongListTileComponent } from './song-list-tile.component';

@NgModule({
    declarations: [SongListTileComponent],
    imports: [CommonModule],
    exports: [SongListTileComponent],
})
export class SongListTileModule {}
