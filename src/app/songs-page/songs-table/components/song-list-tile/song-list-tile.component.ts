import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';
import { SongListItem } from './interfaces/song-list-item.interface';

@Component({
    selector: 'sng-song-list-tile',
    templateUrl: './song-list-tile.component.html',
    styleUrls: ['./song-list-tile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SongListTileComponent {
    @Input() song: SongListItem;

    @Output() addSong = new EventEmitter();
}
