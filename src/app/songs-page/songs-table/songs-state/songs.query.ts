import { Selector } from '@ngxs/store';
import { SongListItemModel } from '../models/song-list-item.model';
import { SongsState } from './songs.state';

export class SongsQuery {
    @Selector([SongsState.items, SongsState.searchQuery])
    static songs(
        items: SongListItemModel[],
        itemsSearchQuery: string
    ): SongListItemModel[] {
        if (itemsSearchQuery) {
            const preparedQuery = itemsSearchQuery.trim().toLowerCase();
            return items.filter(
                x =>
                    x.songName.trim().toLowerCase().includes(preparedQuery) ||
                    x.authorName.trim().toLowerCase().includes(preparedQuery)
            );
        }

        return items;
    }
}
