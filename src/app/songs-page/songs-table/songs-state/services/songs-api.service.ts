import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SongListItemModel } from '../../models/song-list-item.model';

@Injectable()
export class SongsApiService {
    private readonly url = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getAll(): Observable<SongListItemModel[]> {
        return this.http.get<SongListItemModel[]>(`${this.url}/playlist`);
    }
}
