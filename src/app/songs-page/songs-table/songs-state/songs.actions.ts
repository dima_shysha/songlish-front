export class GetSongs {
    static readonly type = '[Songs] Get songs';
}

export class SetSongsSearchQuery {
    static readonly type = '[Songs] Set songs search query';
    constructor(public query: string) {}
}
