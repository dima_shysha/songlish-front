import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Observable, tap } from 'rxjs';
import { GetSongs, SetSongsSearchQuery } from './songs.actions';
import { SongListItemModel } from '../models/song-list-item.model';
import { SongsApiService } from './services/songs-api.service';

export class SongsStateModel {
    public items: SongListItemModel[];
    searchQuery: string;
}

const defaults = {
    items: [],
    searchQuery: null,
} as SongsStateModel;

@State<SongsStateModel>({
    name: 'songs',
    defaults,
})
@Injectable()
export class SongsState {
    constructor(private songsApiService: SongsApiService) {}

    @Selector()
    static items(state: SongsStateModel): SongListItemModel[] {
        return state.items;
    }

    @Selector()
    static searchQuery(state: SongsStateModel): string {
        return state.searchQuery;
    }

    @Action(GetSongs)
    onGetSongs({
        patchState,
    }: StateContext<SongsStateModel>): Observable<SongListItemModel[]> {
        return this.songsApiService.getAll().pipe(
            tap(items => {
                patchState({ items });
            })
        );
    }

    @Action(SetSongsSearchQuery)
    onSetSongsSearchQuery(
        { patchState }: StateContext<SongsStateModel>,
        { query }: SetSongsSearchQuery
    ): void {
        patchState({ searchQuery: query });
    }
}
