import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongsTableComponent } from './songs-table.component';
import { SongsState } from './songs-state/songs.state';
import { NgxsModule } from '@ngxs/store';
import { SongsApiService } from './songs-state/services/songs-api.service';
import { SongListTileModule } from './components/song-list-tile/song-list-tile.module';
import { MatDialogModule } from '@angular/material/dialog';
import { SongDetailsModalModule } from './components/song-details-modal/song-details-modal.module';
import { SearchFieldModule } from 'src/app/words-table/components/search-field/search-field.module';
import { MatButtonModule } from '@angular/material/button';
import { LoadingDirectiveModule } from 'src/app/_shared/loading-directive/loading-directive.module';

@NgModule({
    declarations: [SongsTableComponent],
    providers: [SongsApiService],
    imports: [
        SongListTileModule,
        SongDetailsModalModule,
        SearchFieldModule,
        CommonModule,
        LoadingDirectiveModule,
        MatDialogModule,
        MatButtonModule,
        NgxsModule.forFeature([SongsState]),
    ],
    exports: [SongsTableComponent],
})
export class SongsTableModule {}
