export class SongListItemModel {
    songId: string;
    playlistId: string;
    authorName: string;
    songName: string;
    status: string;

    constructor(init?: Partial<SongListItemModel>) {
        Object.assign(this, init);
    }
}
