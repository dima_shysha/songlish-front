import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmMenuComponent } from './confirm-menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [ConfirmMenuComponent],
    imports: [CommonModule, MatMenuModule, MatButtonModule],
    exports: [ConfirmMenuComponent],
})
export class ConfirmMenuModule {}
