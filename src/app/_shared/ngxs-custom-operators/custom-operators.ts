import { Predicate } from '@ngxs/store/operators/internals';
import { RepairType } from '@ngxs/store/operators/utils';
import { StateOperator } from '@ngxs/store';

export function updateItems<T>(
    selector: number[] | Predicate<T>,
    operatorOrValue: Partial<T> | StateOperator<T>
): StateOperator<T[]> {
    return function updateItemsOperator(existing: Readonly<T[]>): T[] {
        const clone = [...existing];
        let indices = [];
        if (!selector || !operatorOrValue) {
            return clone;
        }
        if (!clone) {
            return clone;
        }
        if (isPredicate(selector)) {
            indices = findIndices(selector, clone as RepairType<T>[]);
        } else if (isArrayNumber(selector)) {
            indices = selector;
        }

        if (invalidIndexs(indices, clone)) {
            return clone;
        }
        let values: Record<number, T> = {};

        if (isStateOperator(operatorOrValue)) {
            values = indices.reduce(
                (acc, it) => ({ ...acc, [it]: operatorOrValue(clone[it]) }),
                {}
            );
        } else {
            values = indices.reduce(
                (acc, it) =>
                    isObject(clone[it])
                        ? { ...acc, [it]: { ...clone[it], ...operatorOrValue } }
                        : { ...acc, [it]: operatorOrValue },
                {}
            );
        }

        const keys = Object.keys(values);
        keys.forEach(value => {
            const key = parseInt(value) ?? 0;
            clone[key] = values[key];
        });
        return clone;
    };
}

export function isPredicate<T>(
    value: Predicate<T> | boolean | number | number[]
): value is Predicate<T> {
    return typeof value === 'function';
}

export function findIndices<T>(
    selector: Predicate<T>,
    existing: RepairType<T>[]
): number[] {
    return existing.reduce((acc, it, i) => {
        const index = selector(it) ? i : -1;
        return invalidIndex(index) ? acc : [...acc, index];
    }, []);
}

export function isArrayNumber(value: number[]): boolean {
    for (const i of value) {
        if (!isNumber(i)) {
            return false;
        }
    }
    return true;
}

export function invalidIndexs<T>(
    indices: number[],
    existing: Readonly<T[]>
): boolean {
    for (const i of indices) {
        if (!existing[i] || !isNumber(i) || invalidIndex(i)) {
            return true;
        }
    }
    return false;
}

export function isStateOperator<T>(
    value: Partial<T> | StateOperator<T>
): value is StateOperator<T> {
    return typeof value === 'function';
}

export function isNumber(value: any): value is number {
    return typeof value === 'number';
}

export function invalidIndex(index: number): boolean {
    return Number.isNaN(index) || index === -1;
}

export function isObject(value: any): boolean {
    return typeof value === 'object';
}

export function isUndefined(value: any): value is undefined {
    return typeof value === 'undefined';
}
