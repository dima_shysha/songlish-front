import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'join',
})
export class JoinPipe implements PipeTransform {
    transform(values: string[], separator: string): string {
        if (!Array.isArray(values)) {
            return values;
        }

        return values.join(separator);
    }
}
