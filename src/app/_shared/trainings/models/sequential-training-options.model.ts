export class SequentialTrainingOptionsModel {
    size: number;
    reversed: boolean;

    constructor(init?: Partial<SequentialTrainingOptionsModel>) {
        Object.assign(this, init);
    }
}
