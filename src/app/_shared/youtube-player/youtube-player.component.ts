import { Component, Input } from '@angular/core';

@Component({
    selector: 'sng-youtube-player',
    templateUrl: './youtube-player.component.html',
    styleUrls: ['./youtube-player.component.scss'],
})
export class YoutubePlayerComponent {
    @Input() src: string;
}
