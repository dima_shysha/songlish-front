import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YoutubePlayerComponent } from './youtube-player.component';
import { SafeYoutubeSrcPipeModule } from './pipes/safe-youtube-src-pipe/safe-youtube-src-pipe.module';

@NgModule({
    declarations: [YoutubePlayerComponent],
    imports: [CommonModule, SafeYoutubeSrcPipeModule],
    exports: [YoutubePlayerComponent],
})
export class YoutubePlayerModule {}
