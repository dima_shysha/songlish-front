import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'safeYoutubeSrc',
})
export class SafeYoutubeSrcPipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) {}

    transform(value: string): unknown {
        return this.getPreparedUrl(value);
    }

    private getPreparedUrl(src: string) {
        const videoId = this.getVideoId(src);
        const preparedUrl = `https://www.youtube.com/embed/${videoId}`;
        return this.sanitizer.bypassSecurityTrustResourceUrl(preparedUrl);
    }

    private getVideoId(url: string) {
        const regExp =
            /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = url.match(regExp);
        return match && match[2].length === 11 ? match[2] : null;
    }
}
