import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeYoutubeSrcPipe } from './safe-youtube-src.pipe';

@NgModule({
    declarations: [SafeYoutubeSrcPipe],
    imports: [CommonModule],
    exports: [SafeYoutubeSrcPipe],
})
export class SafeYoutubeSrcPipeModule {}
