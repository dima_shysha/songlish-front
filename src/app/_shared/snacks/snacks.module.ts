import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgxsModule } from '@ngxs/store';
import { SnackState } from './state/snack.state';

@NgModule({
    declarations: [],
    imports: [
        NgxsModule.forFeature([SnackState]),
        CommonModule,
        MatSnackBarModule,
    ],
})
export class SnacksModule {}
