import { TestBed, async } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { SnackState } from './snack.state';
import { SnackAction } from './snack.actions';

describe('Snack actions', () => {
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([SnackState])],
        }).compileComponents();
        store = TestBed.get(Store);
    }));

    it('should create an action and add an item', () => {
        store.dispatch(new SnackAction('item-1'));
        store
            .select(state => state.snack.items)
            .subscribe((items: string[]) => {
                expect(items).toEqual(jasmine.objectContaining(['item-1']));
            });
    });
});
