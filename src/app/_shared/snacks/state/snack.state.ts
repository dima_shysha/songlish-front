import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { State, Action, StateContext } from '@ngxs/store';
import { OpenErrorSnack, OpenSuccessSnack } from './snack.actions';

export class SnackStateModel {}

const defaults = {};

@State<SnackStateModel>({
    name: 'snack',
    defaults,
})
@Injectable()
export class SnackState {
    constructor(private _snackBar: MatSnackBar) {}

    openSnackBar() {}

    @Action(OpenSuccessSnack)
    onOpenSnack(
        _: StateContext<SnackStateModel>,
        { message }: OpenSuccessSnack
    ) {
        this._snackBar.open(message, '', {
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
            duration: 2000,
            panelClass: ['success-snackbar'],
        });
    }

    @Action(OpenErrorSnack)
    onOpenErrorSnack(
        _: StateContext<SnackStateModel>,
        { message }: OpenErrorSnack
    ) {
        this._snackBar.open(message, '', {
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
            duration: 2000,
            panelClass: ['error-snackbar'],
        });
    }
}
