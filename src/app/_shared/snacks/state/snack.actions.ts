export class OpenSuccessSnack {
    static readonly type = '[Snack] Open success snack';

    constructor(public message: string) {}
}

export class OpenErrorSnack {
    static readonly type = '[Snack] Open error snack';

    constructor(public message: string) {}
}
