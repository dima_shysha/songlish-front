import { fromEvent, map, Observable, startWith } from 'rxjs';

const mobileMaxSize = 480;
const tabletMaxSize = 800;

export class ScreenHelper {
    static isMobile$: Observable<boolean> = fromEvent(window, 'resize').pipe(
        map(x => (x.target as any).innerWidth <= mobileMaxSize),
        startWith(window.innerWidth <= mobileMaxSize)
    );

    static isTablet$: Observable<boolean> = fromEvent(window, 'resize').pipe(
        map(x => (x.target as any).innerWidth <= tabletMaxSize),
        startWith(window.innerWidth <= tabletMaxSize)
    );
}
