import {
    ApplicationRef,
    ComponentFactoryResolver,
    Directive,
    ElementRef,
    EmbeddedViewRef,
    Injector,
    Input,
} from '@angular/core';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';

@Directive({
    selector: '[sngLoading]',
})
export class LoadingDirective {
    @Input()
    public set isLoading(v: boolean) {
        if (v) {
            this.appendLoading();
            return;
        }

        this.removeLoading();
    }

    private loadingElem: HTMLElement;

    constructor(
        private el: ElementRef,
        private componentFactoryResolver: ComponentFactoryResolver,
        private injector: Injector,
        private appRef: ApplicationRef
    ) {
        this.initLoadingElem();
    }

    private appendLoading(): void {
        (<HTMLElement>this.el.nativeElement).appendChild(this.loadingElem);
    }

    private removeLoading(): void {
        (<HTMLElement>this.el.nativeElement).removeChild(this.loadingElem);
    }

    private initLoadingElem(): void {
        const componentFactory =
            this.componentFactoryResolver.resolveComponentFactory(
                LoadingSpinnerComponent
            );

        const componentRef = componentFactory.create(this.injector);
        this.appRef.attachView(componentRef.hostView);
        this.loadingElem = (componentRef.hostView as EmbeddedViewRef<any>)
            .rootNodes[0] as HTMLElement;
    }
}
