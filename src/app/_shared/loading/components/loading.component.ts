import { Component } from '@angular/core';

@Component({
    selector: 'sng-loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent {
    message: string;
}
