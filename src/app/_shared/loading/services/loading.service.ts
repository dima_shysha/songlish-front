import { OverlayRef, Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { LoadingComponent } from '../components/loading.component';

export class LoadingOverlayRef {
    constructor(private overlayRef: OverlayRef) {}

    close(): void {
        this.overlayRef.dispose();
    }
}

@Injectable()
export class LoadingService {
    constructor(private overlay: Overlay) {}

    message: string;

    loadingRef: LoadingOverlayRef;

    open(message?: string): void {
        this.message = message;
        const overlayRef = this.createOverlay();
        const dialogRef = new LoadingOverlayRef(overlayRef);
        const overlayComponent = this.attachDialogContainer(overlayRef);
        overlayComponent.message = message;

        this.loadingRef = dialogRef;
    }

    close(): void {
        this.loadingRef.close();
    }

    private createOverlay(): OverlayRef {
        const positionStrategy = this.overlay
            .position()
            .global()
            .centerHorizontally()
            .centerVertically();
        const overlayConfig = new OverlayConfig({
            hasBackdrop: true,
            scrollStrategy: this.overlay.scrollStrategies.block(),
            positionStrategy,
            backdropClass: 'loading-backdrop',
        });

        return this.overlay.create(overlayConfig);
    }

    private attachDialogContainer(overlayRef: OverlayRef): LoadingComponent {
        const containerPortal = new ComponentPortal(LoadingComponent, null);
        const containerRef = overlayRef.attach(containerPortal);

        return containerRef.instance;
    }
}
