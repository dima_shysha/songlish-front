import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoadingComponent } from './components/loading.component';
import { LoadingService } from './services/loading.service';

@NgModule({
    declarations: [LoadingComponent],
    providers: [LoadingService],
    imports: [MatProgressSpinnerModule],
})
export class LoadingModule {}
