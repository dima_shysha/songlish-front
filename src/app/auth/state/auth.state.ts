import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { catchError, filter, Observable, of, tap } from 'rxjs';
import { AuthApiService } from '../services/auth-api.service';
import { Login, Logout, Register, ResetAuthErrors } from './auth.actions';

export class AuthStateModel {
    loginErr: boolean;
    signupErr: boolean;
}

const defaults = {
    loginErr: false,
    signupErr: false,
} as AuthStateModel;

@State<AuthStateModel>({
    name: 'auth',
    defaults,
})
@Injectable()
export class AuthState {
    constructor(private authApiService: AuthApiService) {}

    @Selector()
    static loginErr(state: AuthStateModel): boolean {
        return state.loginErr;
    }

    @Selector()
    static signupErr(state: AuthStateModel): boolean {
        return state.signupErr;
    }

    @Action(Register)
    onRegister(
        { dispatch, patchState }: StateContext<AuthStateModel>,
        { payload }: Register
    ): Observable<any> {
        return this.authApiService.register(payload).pipe(
            catchError(err => {
                if (err.status === 400) {
                    patchState({
                        signupErr: true,
                    });
                }

                return of(null);
            }),
            filter(x => !!x),
            tap(x => {
                if (x) {
                    dispatch(new Navigate(['duh']));
                    return;
                }
            })
        );
    }

    @Action(Login)
    onLogin(
        { dispatch, patchState }: StateContext<AuthStateModel>,
        { payload }: Login
    ): Observable<any> {
        return this.authApiService.login(payload).pipe(
            catchError(err => {
                if (err.status === 400) {
                    patchState({
                        loginErr: true,
                    });
                }

                return of(null);
            }),
            filter(x => !!x),
            tap(() => {
                dispatch(new Navigate(['duh']));
                return;
            })
        );
    }

    @Action(Logout)
    onLogout(_: StateContext<AuthStateModel>, __: Login): Observable<any> {
        return this.authApiService.logout();
    }

    @Action(ResetAuthErrors)
    onResetAuthErrors({ setState }: StateContext<AuthStateModel>): void {
        setState(defaults);
    }
}
