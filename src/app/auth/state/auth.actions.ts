import { LoginDataModel } from '../components/login-page/models/login-data.model';
import { RegistrationDataModel } from '../components/registration-page/models/registration-data.model';

export class Register {
    static readonly type = '[Auth] Register';

    constructor(public payload: RegistrationDataModel) {}
}

export class Login {
    static readonly type = '[Auth] Login';

    constructor(public payload: LoginDataModel) {}
}

export class Logout {
    static readonly type = '[Auth] Logout';
}

export class ResetAuthErrors {
    static readonly type = '[Auth] Reset auth errors';
}
