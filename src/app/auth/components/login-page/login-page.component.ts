import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Login, ResetAuthErrors } from '../../state/auth.actions';
import { AuthState } from '../../state/auth.state';
import { LoginDataModel } from './models/login-data.model';

@Component({
    selector: 'sng-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, OnDestroy {
    @Select(AuthState.loginErr) loginErr$: Observable<boolean>;

    form: FormGroup;

    hidePassword = true;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.form = new FormGroup({
            email: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
        });
    }

    ngOnDestroy(): void {
        this.store.dispatch(new ResetAuthErrors());
    }

    onFormSubmit(): void {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
            return;
        }

        let formModel = this.form.getRawValue();
        let model = new LoginDataModel({
            email: formModel.email,
            password: formModel.password,
        });
        this.store.dispatch(new Login(model));
    }
}
