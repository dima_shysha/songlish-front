export class LoginDataModel {
    email: string;
    password: string;

    constructor(init?: Partial<LoginDataModel>) {
        Object.assign(this, init);
    }
}
