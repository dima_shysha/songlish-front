export class RegistrationDataModel {
    email: string;
    password: string;

    constructor(init?: Partial<RegistrationDataModel>) {
        Object.assign(this, init);
    }
}
