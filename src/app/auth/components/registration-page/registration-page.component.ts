import { Component, OnDestroy, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormControl,
    FormGroup,
    ValidationErrors,
    Validators,
} from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Register, ResetAuthErrors } from '../../state/auth.actions';
import { AuthState } from '../../state/auth.state';
import { RegistrationDataModel } from './models/registration-data.model';

@Component({
    selector: 'sng-registration-page',
    templateUrl: './registration-page.component.html',
    styleUrls: ['./registration-page.component.scss'],
})
export class RegistrationPageComponent implements OnInit, OnDestroy {
    @Select(AuthState.signupErr) signupErr$: Observable<boolean>;

    form: FormGroup;

    hidePassword = true;

    constructor(private store: Store) {}

    confirmPasswordValidator = (
        control: AbstractControl
    ): ValidationErrors | null => {
        return this.form?.get('password').value !== control.value
            ? { passwordConfirmed: true }
            : null;
    };

    ngOnInit(): void {
        this.form = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', [
                Validators.required,
                Validators.minLength(8),
            ]),
            confirmPassword: new FormControl('', [
                Validators.required,
                this.confirmPasswordValidator,
            ]),
        });
    }

    ngOnDestroy(): void {
        this.store.dispatch(new ResetAuthErrors());
    }

    onFormSubmit(): void {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
            return;
        }

        let formModel = this.form.getRawValue();
        let model = new RegistrationDataModel({
            email: formModel.email,
            password: formModel.password,
        });
        this.store.dispatch(new Register(model));
    }
}
