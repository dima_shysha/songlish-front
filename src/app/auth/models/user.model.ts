export class UserModel {
    id: number;
    email: string;
    password: string;
    token?: string;
}
