export class RefreshTokenModel {
    accessToken: string;
    refreshToken: string;

    constructor(init?: Partial<RefreshTokenModel>) {
        Object.assign(this, init);
    }
}
