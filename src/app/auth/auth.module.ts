import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { NgxsModule } from '@ngxs/store';
import { AuthState } from './state/auth.state';
import { MatIconModule } from '@angular/material/icon';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegistrationPageComponent } from './components/registration-page/registration-page.component';

@NgModule({
    declarations: [RegistrationPageComponent, LoginPageComponent],
    imports: [
        NgxsModule.forFeature([AuthState]),
        CommonModule,
        MatButtonModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatIconModule
    ],
    exports: [RegistrationPageComponent, LoginPageComponent],
})
export class RegistrationModule {}
