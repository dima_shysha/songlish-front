import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { map, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginDataModel } from '../components/login-page/models/login-data.model';
import { RegistrationDataModel } from '../components/registration-page/models/registration-data.model';
import { RefreshTokenModel } from '../models/refresh-token.model';

@Injectable({
    providedIn: 'root',
})
export class AuthApiService {
    private readonly url = environment.apiUrl;

    private http: HttpClient;

    private _accessToken = '';
    getAccessToken(): string {
        if (!this._accessToken) {
            return this.cookieService.get('accessToken');
        }

        return this._accessToken;
    }

    constructor(private cookieService: CookieService, handler: HttpBackend) {
        this.http = new HttpClient(handler);
    }

    register(model: RegistrationDataModel): Observable<any> {
        return this.http.post<any>(`${this.url}/auth/signup`, model).pipe(
            map(user => {
                this._accessToken = user.accessToken;
                this.cookieService.set('accessToken', user.accessToken);
                this.cookieService.set('refreshToken', user.refreshToken);
                return user;
            })
        );
    }

    login(model: LoginDataModel) {
        return this.http.post<any>(`${this.url}/auth/login`, model).pipe(
            tap(user => {
                this._accessToken = user.accessToken;
                this.cookieService.set('accessToken', user.accessToken);
                this.cookieService.set('refreshToken', user.refreshToken);
            })
        );
    }

    logout(): Observable<any> {
        return this.http.post(`${this.url}/auth/logout`, null).pipe(
            tap(() => {
                this.cookieService.delete('accessToken');
                this.cookieService.delete('refreshToken');
                return of(true);
            })
        );
    }

    refreshToken(): Observable<RefreshTokenModel> {
        const newLocal = this.cookieService.get('refreshToken');
        return this.http
            .post<RefreshTokenModel>(`${this.url}/auth/refresh`, {
                refreshToken: newLocal,
            })
            .pipe(
                tap(user => {
                    this._accessToken = user.accessToken;
                    this.cookieService.set('accessToken', user.accessToken);
                    this.cookieService.set('refreshToken', user.refreshToken);
                })
            );
    }
}
