import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
} from '@angular/common/http';
import {
    catchError,
    filter,
    Observable,
    of,
    switchMap,
    take,
    throwError,
} from 'rxjs';
import { AuthApiService } from '../services/auth-api.service';
import { RefreshTokenModel } from '../models/refresh-token.model';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthApiService, private store: Store) {}

    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        return next
            .handle(
                request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${this.authService.getAccessToken()}`,
                    },
                })
            )
            .pipe(
                catchError(err => {
                    if (err.status !== 400) {
                        return throwError(() => err);
                    }

                    return this.authService.refreshToken().pipe(
                        take(1),
                        catchError(err => {
                            if (err.status === 400) {
                                this.store.dispatch(new Navigate(['/login']));
                            }

                            return of(null);
                        }),
                        filter(x => !!x),
                        switchMap((tokenModel: RefreshTokenModel) =>
                            next.handle(
                                request.clone({
                                    setHeaders: {
                                        Authorization: `Bearer ${tokenModel.accessToken}`,
                                    },
                                })
                            )
                        )
                    );
                })
            );
    }
}
